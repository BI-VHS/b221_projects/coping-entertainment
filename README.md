# Coping Entertainment

## linky na gitlab wiki stránky projektu:

- [GDD](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/GDD)
- [GDD v2](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/GDD-v2)
- [Full notes](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/Full-notes)
- [Dokumentace: kód](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/Dokumentace-k%C3%B3du)
- [Dokumentace: statický svět](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/Dokumentace-Statick%C3%BD-Sv%C4%9Bt)
- [Dokumentace: dynamický svět](https://gitlab.fit.cvut.cz/BI-VHS/b221_projects/coping-entertainment/-/wikis/Dokumentacia-dynamic-world)

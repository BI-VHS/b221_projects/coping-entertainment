using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LoadingScreen : MonoBehaviour
{
    /// <summary>
    /// Call when unloading scene and loading new one started
    /// </summary>
    public void StartLoading()
    {
        
    }

    /// <summary>
    /// Call when loading scene finished.
    /// Loading screen may still persist (animation for example), but wont end before EndLoading is called
    /// TODO, do not integrate yet.
    /// </summary>
    public void EndLoading()
    {
        
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

public class SettingsView : MonoBehaviour
{
    private Settings _settings;
    
    private void Awake()
    {
        _settings = new Settings();
        gameObject.SetActive(false);
    }

    public void LoadValues()
    {
        
    }

    public void SaveValues()
    {
        
    }


    private void OnDisable()
    {
        Settings.Save();
    }
}

/// <summary>
/// Get, set and manage variables 
/// </summary>
public class Settings
{
    private static Dictionary<string, object> values;

    public Settings()
    {
        values = new Dictionary<string, object>();
    }

    /// <summary>
    /// Loads(initializes) setting with key identifier.
    /// If no value has been saved yet, initialize to valueDefault
    /// </summary>
    /// <typeparam name="T">bool, int, float or string or Vector*</typeparam>
    public static T InitializePref<T>(string key, T valueDefault)
    {
        // iff not exists, use and save default value
        // if does, dont
        //values[key] = PlayerPrefs.GetInt(key, valueDefault);
        //PlayerPrefs.SetInt(key, (int)values[key]);

        T obtainedValue = GetFromPrefs<T>(key, (T)valueDefault);
        Set<T>(key, obtainedValue);
        return obtainedValue;
    }

    private static T GetFromPrefs<T>(string key, T valueDefault)
    {
        object result = valueDefault switch
        {
            int t => PlayerPrefs.GetInt(key, t),
            float t => PlayerPrefs.GetFloat(key, t),
            string t => PlayerPrefs.GetString(key, t),
            bool t => Convert.ToBoolean(PlayerPrefs.GetInt(key, Convert.ToInt32(t))),
            Vector2 t => new Vector2(
                GetFromPrefs(key + "_vec2_x", t.x),
                GetFromPrefs(key + "_vec2_y", t.y)),
            Vector3 t => new Vector3(
                GetFromPrefs(key + "_vec3_x", t.x),
                GetFromPrefs(key + "_vec3_y", t.y),
                GetFromPrefs(key + "_vec3_z", t.z)),
            Vector2Int t => new Vector2Int(
                GetFromPrefs(key + "_vec2int_x", t.x),
                GetFromPrefs(key + "_vec2int_y", t.y)
            ),
            Vector3Int t => new Vector3Int(
                GetFromPrefs(key + "_vec3int_x", t.x),
                GetFromPrefs(key + "_vec3int_y", t.y),
                GetFromPrefs(key + "_vec3int_z", t.z)
            ),
            int[] t => Enumerable.Range(0, t.Length).Select(i => GetFromPrefs(key + $"_intarray_{i}", t[i])).ToArray(),
            float[] t => Enumerable.Range(0, t.Length).Select(i => GetFromPrefs(key + $"_floatarray_{i}", t[i])).ToArray(),
            Enum t => PlayerPrefs.GetInt(key),
            _ => throw new TypeAccessException()
        };
        ;
        return (T)result;
    }

    /// <summary>
    /// Gets current value of setting using the key
    /// </summary>
    /// <typeparam name="T">bool, int, float or string or Vector*</typeparam>
    public static T Get<T>(string key)
    {
        return (T) values[key];
    }
    
    /// <summary>
    /// Sets and saves key value.
    /// </summary>
    /// <typeparam name="T">bool, int, float or string or Vector*</typeparam>
    public static void Set<T>(string key, T value)
    {
        values[key] = value;

        switch (value)
        {
            case bool t:
                PlayerPrefs.SetInt(key, Convert.ToInt32(t));
                break;
            case int t:
                PlayerPrefs.SetInt(key, t);
                break;
            case float t:
                PlayerPrefs.SetFloat(key, t);
                break;
            case string t:
                PlayerPrefs.SetString(key, t);
                break;
            case Vector2 t:
                PlayerPrefs.SetFloat(key + "_vec2_x", t.x);
                PlayerPrefs.SetFloat(key + "_vec2_y", t.y);
                break;
            case Vector3 t:
                PlayerPrefs.SetFloat(key + "_vec3_x", t.x);
                PlayerPrefs.SetFloat(key + "_vec3_y", t.y);
                PlayerPrefs.SetFloat(key + "_vec3_z", t.z);
                break;
            case Vector2Int t:
                PlayerPrefs.SetFloat(key + "_vec2int_x", t.x);
                PlayerPrefs.SetFloat(key + "_vec2int_y", t.y);
                break;
            case Vector3Int t:
                PlayerPrefs.SetFloat(key + "_vec3int_x", t.x);
                PlayerPrefs.SetFloat(key + "_vec3int_y", t.y);
                PlayerPrefs.SetFloat(key + "_vec3int_z", t.z);
                break;
            case int[] t:
                for (int i = 0; i < t.Length; i++)
                {
                    PlayerPrefs.SetInt(key + $"_intarray_{i}", t[i]);
                }
                break;
            case float[] t:
                for (int i = 0; i < t.Length; i++)
                {
                    PlayerPrefs.SetFloat(key + $"_floatarray_{i}", t[i]);
                }
                break;
            default:
                Debug.LogError("Cannot save value: unsupported value type");
                break;
        }
        
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }
    
    ~Settings(){
        Save();
    }
    
}

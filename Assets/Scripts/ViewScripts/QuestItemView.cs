using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestItemView : MonoBehaviour
{
    public string description;

    public void OnTooltip(bool active)
    {
        if (active)
        {
            Tooltip.Show(description);
        }
        else
        {
            Tooltip.Hide();
        }
    }


}

using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    public TextMeshProUGUI descriptionText;

    private float showTime;

    private static Tooltip instance;
    
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        Hide();
    }

    public static void Show(string description)
    {
        instance.gameObject.SetActive(true);

        var dt = instance.descriptionText;
        
        instance.descriptionText.text = description;
        
        // recalc size
        dt.enableAutoSizing = true;
        
        // set timer to inf
        instance.showTime = float.PositiveInfinity;
        
        // show warnings
        if (description.Length > 130)
        {
            Debug.LogAssertion("Tooltip description may be overflowing! Try to keep it to max 130 chars.");
        }
    }

    public static void Show(string description, float messageTimeLength)
    {
        Show(description);
        instance.showTime = messageTimeLength;
    }

    public static void Hide()
    {
        instance.gameObject.SetActive(false);
    }

    private int minTextSize => Screen.height / 1080 * 30;
    
    // Update is called once per frame
    void Update()
    {
        if (descriptionText.enableAutoSizing && descriptionText.fontSize < minTextSize)
        {
            descriptionText.enableAutoSizing = false;
            descriptionText.fontSize = minTextSize;
        }

        if (showTime > 0)
        {
            showTime -= Time.deltaTime;
            if (showTime <= 0)
            {
                Hide();
            }
        }
    }
}

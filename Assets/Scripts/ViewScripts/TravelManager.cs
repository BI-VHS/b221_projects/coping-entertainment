using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TravelManager : MonoBehaviour
{
    public GameObject wrapper;
    
    
    // Start is called before the first frame update
    void Start()
    {
        wrapper.SetActive(false);
    }

    public void GoToLevel(int level)
    {
        UIManager.instance.LoadScene(level);
    }

    // Update is called once per frame
    void Update()
    {
        if (Controls.GetButtonDownGame("debug"))
        {
            wrapper.SetActive(true);
        }

        if (Controls.GetButtonUpGame("debug"))
        {
            wrapper.SetActive(false);
        }
    }
}

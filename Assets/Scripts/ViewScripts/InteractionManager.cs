using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum InteractionResult
{
    cancel = 0, // esc key
    negative = 100, positive = 200 // z and c keys
}

public class InteractionManager : MonoBehaviour
{
    public GameObject wrapper;
    
    public Button ESCButton;
    
    public TextMeshProUGUI zButtonText;
    public TextMeshProUGUI cButtonText;
    public GameObject buttons_object;
    
    public Button zButton;
    public Button cButton;
    public GameObject escButton;

    public TextMeshProUGUI title;
    
    public TextMeshProUGUI description_single;
    public GameObject description_single_object;

    public TextMeshProUGUI comparison_leftcolumn;

    public Image comparison_ours_image;
    public Image comparison_theirs_image;
    
    public TextMeshProUGUI comparison_ours_title;
    public TextMeshProUGUI comparison_theirs_title;
    
    public TextMeshProUGUI comparison_ours;
    public TextMeshProUGUI comparison_theirs;
    
    public GameObject comparison_object;

    /// <summary>
    /// Delegate used to callback
    /// </summary>
    public delegate void InteractionComplete(InteractionResult interactionResult);

    private InteractionComplete runWhenCompleteFunction;

    private bool[] optionsAvaliable;

    public static InteractionManager instance;
    
    private void Awake()
    {
        optionsAvaliable = new[] {false, false, false};
        
        wrapper.SetActive(false);
        instance = this;
    }

    private void Update()
    {
        if (Controls.GetButtonDownUI("Cancel"))
        {
            Tooltip.Hide(); // just to be sure
            OnAction(InteractionResult.cancel);
        }

        else if (Controls.GetButtonDownUI("leftoption")) // z)
        {
            Tooltip.Hide(); // just to be sure
            OnAction(InteractionResult.negative);
        }

        else if (Controls.GetButtonDownUI("rightoption")) // c)
        {
            Tooltip.Hide(); // just to be sure
            OnAction(InteractionResult.positive);
        }
    }

    private void Rescale()
    {
        float textSize = (float)Screen.height / 35;

        description_single.fontSize = textSize * 1.25f;
        comparison_ours.fontSize = textSize;
        comparison_theirs.fontSize = textSize;
        comparison_leftcolumn.fontSize = textSize;
    }

    private void ToggleComponents(bool show_description, bool show_comparison, bool show_buttons)
    {
        wrapper.SetActive(true);
        description_single_object.SetActive(show_description);
        comparison_object.SetActive(show_comparison);
        buttons_object.SetActive(show_buttons);

        zButton.interactable = true;
        cButton.interactable = true;
        escButton.SetActive(true);

        // disables player controls
        UIManager.currentMode = HUDMode.UI;
        
        Rescale();
    }

    /// <summary>
    /// Hotfix tbh
    /// </summary>
    private void SetOptions(bool esc, bool negativeZ, bool positiveC)
    {
        optionsAvaliable = new[] {esc, negativeZ, positiveC};
    }

    private string FormatWeaponStats(WeaponData weaponData)
    {
        string ammoName = GameState.instance.ammoData[(int) weaponData.maximumCaliber].nameLong;

        int ROF = (int)(60 / weaponData.intraclip_nominal);
        int ACC = (int)(100 / (weaponData.dispersion_nominal + 1));
        
        return $"{ammoName}\n" +
               $"{weaponData.interclip} s\n" +
               $"{ROF} rpm\n" +
               $"{weaponData.clip_nominal} pcs\n" +
               $"{ACC}/100\n" +
               $"<i>{weaponData.description}</i>";
    }

    /// <summary>
    /// Shows only lore
    /// </summary>
    public void ShowLore(InteractionComplete callbackFunction, string itemName, string itemDescription)
    {
        ToggleComponents(true, false, false);
        SetOptions(true, true, true);

        title.text = itemName;
        description_single.text = itemDescription;

        runWhenCompleteFunction = callbackFunction;
    }

    public void ShowLoreWrapped(string itemName, string itemDescription)
    {
        ShowLore(_ => {}, itemName, itemDescription);
    }

    public void ShowCollectibleItem(InteractionComplete callbackFunction, string itemName, string itemDescription)
    {
        // show: esc, title, desc, buttons
        ToggleComponents(true, false, true);
        SetOptions(true, true, true);

        title.text = itemName;
        description_single.text = itemDescription;

        zButtonText.text = $"z) Leave";
        cButtonText.text = $"c) Collect";

        runWhenCompleteFunction = callbackFunction;
    }

    public void ShowDialogue(string npcName, string npcDialogue,
        string responseNegative, string responsePositive, InteractionComplete callbackFunction)
    {
        bool hasSingleOption = responseNegative == "";
        
        // show esc, title, desc, buttonReplies, buttons
        ToggleComponents(true, false, true);
        
        // actually, dont show ESC
        escButton.SetActive(false);
        
        title.text = npcName;

        string zOptionDesc = hasSingleOption ? "" : $"z) {responseNegative}";
        description_single.text = $"{npcDialogue}\n\n-------------\n{zOptionDesc}\nc) {responsePositive}";

        
        zButtonText.text = hasSingleOption ? "" : "z) ";
        cButtonText.text = $"c) ";

        zButton.interactable =! hasSingleOption;

        SetOptions(false, zButton.interactable, true);
        runWhenCompleteFunction = callbackFunction;
    }

    public void ShowWeapon(InteractionComplete callbackFunction, WeaponData weaponPlayer, WeaponData weaponPickup)
    {
        // show esc, weaponIcons, weaponTitles, weaponDesc, weaponDescLeftColumn, buttons
        ToggleComponents(false, true, true);
        SetOptions(true, true, true);

        title.text = "Switch weapon for new one?";

        comparison_ours_image.sprite = weaponPlayer.sprite;
        comparison_theirs_image.sprite = weaponPickup.sprite;

        comparison_ours_title.text = $"{weaponPlayer.weaponName} (old)";
        comparison_theirs_title.text = $"{weaponPickup.weaponName} (new)";

        comparison_ours.text = FormatWeaponStats(weaponPlayer);
        comparison_theirs.text = FormatWeaponStats(weaponPickup);

        zButtonText.text = "z) Keep old (left)";
        cButtonText.text = "c) Take new (right)";

        runWhenCompleteFunction = callbackFunction;
    }

    /// <summary>
    /// Call this when player picks up weapon
    /// </summary>
    public void ShowWeaponWrapped(WeaponData weaponPickup)
    {
        WeaponData weaponPlayer = GameState.instance.weapon;
        ShowWeapon(result =>
        {
            if (result == InteractionResult.positive)
            {
                GameState.instance.weapon = weaponPickup;
            }
        }, weaponPlayer, weaponPickup);
    }

    public void BuyWeapon(InteractionComplete callbackFunction, WeaponData weaponPlayer, WeaponData weaponPickup, AmmoType priceType, int price)
    {
        // show esc, same as above, but also gray out button possibly
        ShowWeapon(callbackFunction, weaponPlayer, weaponPickup);


        string ammoName = priceType == AmmoType.mm556 ? "5.56mm" : priceType == AmmoType.mm762 ? "7.62mm" : "8.58mm"; // TODO nicer
        title.text = $"Buy weapon for {price}x {ammoName}";

        zButtonText.text = "z) Keep left (old)";
        cButtonText.text = "c) Buy right (new)";

        int playerAmmo = GameState.instance.ammoData[(int) priceType].amount;
        cButton.interactable = playerAmmo >= price;
        
        SetOptions(true, true, cButton.interactable);
    }

    /// <summary>
    /// Use this to make an entire trade with a weapon
    /// </summary>
    public void BuyWeaponWrapped(WeaponData weaponPickup, AmmoType priceType, int price)
    {
        WeaponData weaponPlayer = GameState.instance.weapon;
        BuyWeapon(result =>
        {
            if (result == InteractionResult.positive)
            {
                GameState.instance.weapon = weaponPickup;
                GameState.instance.ChangeInAmmoCount(priceType, -1 * price);
                GameState.instance.UpdateUI();
            }
        }, weaponPlayer, weaponPickup, priceType, price);
    }

    public void BuyItem(InteractionComplete callbackFunction, string itemName, string itemDescription, AmmoType priceType, int price)
    {
        //show esc, title, description, buttons w/price
        ShowCollectibleItem(callbackFunction, itemName, itemDescription);

        zButtonText.text = "z) Cancel trade";
        cButtonText.text = "c) Buy";

        int playerAmmo = GameState.instance.ammoData[(int) priceType].amount;
        cButton.interactable = playerAmmo >= price;
        
        SetOptions(true, true, cButton.interactable);
    }

    /// <summary>
    /// Used internally to handle tooltip for the three buttons
    /// TODO extend to weapon stats
    /// </summary>
    public void TooltipShow(string description)
    {
        Tooltip.Show(description);
    }
    
    public void TooltipHide()
    {
        Tooltip.Hide();
    }

    public void OnAction(int interactionResult)
    {
        OnAction((InteractionResult)interactionResult);
    }
    
    public void OnAction(InteractionResult interactionResult)
    {
        wrapper.SetActive(false);
        
        //
        UIManager.currentMode = HUDMode.game;
        
        // callback
        runWhenCompleteFunction(interactionResult);
    }
    
}


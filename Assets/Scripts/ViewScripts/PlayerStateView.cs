using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStateView : MonoBehaviour
{
    public Image healthBar;

    public ItemView weapon;
    
    public ItemView[] ammoViews;
    
    public static PlayerStateView instance;

    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;

    }

    public void SetHealth(int newHealth, int maxHealth)
    {
        healthBar.fillAmount = (float)newHealth / maxHealth;
    }

    /// <summary>
    /// 0 - weapon just started loading
    /// 1 - weapon just finished loading
    /// </summary>
    public void SetWeaponLoadStatus(float loadStatus)
    {
        weapon.loadStatus = 1 - loadStatus;
    }

    /// <summary>
    /// Set how much reminaing clip
    /// </summary>
    public void SetWeaponClipRemaining(int clipCurrent, int clipTotal)
    {
        float clipRatio = (float) clipCurrent / clipTotal;
        weapon.clipRemainingRatio = clipCurrent == 0 ? 1 : clipRatio;

        weapon.label = $"{clipCurrent}/{clipTotal}";
    }
    
    /// <summary>
    /// Sets the weapon item in the UI
    /// </summary>
    public void SetWeapon(WeaponData weaponData)
    {
        weapon.description = weaponData.description;
        // weapon.label = weaponData.weaponName;
        // weapon.label = $"{weaponData.clip_real()}";

        for (int i = 0; i < GameState.ammoTypesCount; i++)
        {
            ammoViews[i].ammoLoadStatus = (int)weaponData.maximumCaliber >= i ? 
                UseStatus.available : UseStatus.unavailable;
        }
    }

    /// <summary>
    /// Select active ammo.
    /// TODO: call when is switched
    /// </summary>
    public void SetActiveAmmo(UseStatus[] useStata)
    {
        for (int i = 0; i < useStata.Length; i++)
        {
            ammoViews[i].ammoLoadStatus = useStata[i];
        }
    }

    /// <summary>
    /// Changes ammo count of selected ammo.
    /// If zero, turns off given button 
    /// </summary>
    public void SetAmmoCount(AmmoType ammoType, int count)
    {
        ammoViews[(int)ammoType].label = $"{count}";
    }

    /// <summary>
    /// Set all ammo counts simultaneously
    /// </summary>
    public void SetAmmoDataInView(AmmoData[] ammoDatas)
    {
        for (int i = 0; i < ammoDatas.Length; i++)
        {
            var avi = ammoViews[i];
            var adi = ammoDatas[i];
            avi.label = $"{adi.amount}";
            avi.description = $"{adi.nameLong}\nDamage: {adi.damage}";
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class QuestView : MonoBehaviour
{
    public GameObject prefab;

    public Transform playerTransform;
    
    private float fontSize => (float) Screen.height / 28;
    private float itemSize => fontSize * 2.5f;
    private float itemOffset => itemSize + itemSize / 4;

    private List<GameObject> currentItems;
    private List<QuestData> currentlySeenQuests;
    private float timeUntilUpdate;

    public static QuestView instance;

    private void Awake()
    {
        instance = this;
    }

    public void UpdateView(List<QuestData> questDatas)
    {
        // TODO make better
        if (PlayerController.instance == null)
        {
            Debug.Log("Trying to get position from null PlayerController!");
            return;
        }
        
        // first delete all
        if (currentItems != null)
        {
            for (int i = 0; i < currentItems.Count; i++)
            {
                Destroy(currentItems[i]);
            }
        }
        
        // Debug.Log($"Creating new quest list with {questDatas.Count} items...");

        // save for periodic updates
        currentlySeenQuests = questDatas;
        
        // create new
        currentItems = new List<GameObject>();

        // populate
        for (int i = 0; i < questDatas.Count; i++)
        {
            var newItem = Instantiate(prefab, transform) as GameObject;
            newItem.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, i * itemOffset);
            newItem.GetComponent<RectTransform>().sizeDelta = new Vector2(0, itemSize);
            newItem.GetComponent<QuestItemView>().description = questDatas[i].description;

            string questPrefix = questDatas[i].specialQuestResult != SpecialQuestResult.none ? "**" : "*";
            string questDisplayName = questDatas[i].questName;
            
            Vector3 distanceVect = questDatas[i].waypoint.Item2 - playerTransform.position;
            distanceVect.y = 0;
            float distance = distanceVect.magnitude;
            
            string goalLevelName = UIManager.GetSceneName();
            string questLine2 = questDatas[i].waypoint.Item1 == UIManager.instance.currentLevel || 
                                (questDatas[i].waypoint.Item1 == 1 && UIManager.instance.currentLevel == 1)
                ? $"Distance: {(int)distance}"
                : $"Waypoint: {goalLevelName}";
            
            //Debug.Log($"wi1: {questDatas[i].waypoint.Item1}, cl: {UIManager.instance.currentLevel}");
            
            TextMeshProUGUI questText = newItem.GetComponent<TextMeshProUGUI>();
            
            questText.text = $"{questPrefix} {questDisplayName}\n   {questLine2}";
            questText.fontSize = fontSize;

            currentItems.Add(newItem);
        }

        timeUntilUpdate = 1f;

    }

    /// <summary>
    /// TODO MORE EFFICIENT
    /// </summary>
    private void Update()
    {
        if (timeUntilUpdate > 0)
        {
            timeUntilUpdate -= Time.deltaTime;
            if (timeUntilUpdate <= 0)
            {
                UpdateView(currentlySeenQuests);
            }
        }
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CotrolsView : MonoBehaviour
{
    
    public GameObject wrapper;
    
    // Start is called before the first frame update
    void Start()
    {
        wrapper.SetActive(false);
    }

    public void Show()
    {
        wrapper.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Controls.GetButtonDownGame("controls"))
        {
            wrapper.SetActive(true);
        }

        if (Controls.GetButtonUpGame("controls"))
        {
            wrapper.SetActive(false);
        }
    }
}

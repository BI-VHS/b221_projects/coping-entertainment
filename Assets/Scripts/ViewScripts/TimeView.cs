using System;
using TMPro;
using UnityEngine;

namespace ViewScripts
{
    public class TimeView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI timerText;

        // Update is called once per frame
        void Update()
        {
            GameTime time = WorldTime.GetTime();

            int hours = (int) time.hour;
            int minutes = (int)((time.hour % 1) * 60);

            timerText.text = $"Day {time.day}, {hours:D2}:{minutes:D2}";

        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public enum UseStatus
{
    active, available, unavailable
}

public class ItemView : MonoBehaviour
{

    [SerializeField]
    private Image image;
    [SerializeField]
    private TextMeshProUGUI labelText;
    [SerializeField]
    private Image backgroundLoadIndicator;

    /// <summary>
    /// 0 - fully unloaded
    /// 1 - fully loaded
    /// </summary>
    public float loadStatus
    {
        private get { throw new NotImplementedException(); }
        set => backgroundLoadIndicator.fillAmount = value;
    }

    public float clipRemainingRatio
    {
        set
        {
            var tc = backgroundLoadIndicator.color;
            tc.a = value;
            backgroundLoadIndicator.color = tc;
        }
    }
    
    /// <summary>
    /// sets the label in the lower right corner of the item
    /// </summary>
    public string label
    {
        private get { throw new NotImplementedException(); }
        set => labelText.text = value;
    }

    /// <summary>
    /// What to show in tooltip
    /// </summary>
    public string description { get; set; }

    public UseStatus ammoLoadStatus
    {
        private get { throw new NotImplementedException(); }
        set
        {
            Color blic = backgroundLoadIndicator.color;
            switch (value)
            {
                case UseStatus.active:
                    backgroundLoadIndicator.fillAmount = 1f;
                    backgroundLoadIndicator.color = new Color(blic.r, blic.g, blic.b, 1f);
                    break;
                case UseStatus.available:
                    backgroundLoadIndicator.fillAmount = 1f;
                    backgroundLoadIndicator.color = new Color(blic.r, blic.g, blic.b, 0.6f);
                    break;
                case UseStatus.unavailable:
                    backgroundLoadIndicator.fillAmount = 0f;
                    backgroundLoadIndicator.color = new Color(blic.r, blic.g, blic.b, 0f);
                    break;
            }
        }
    }

    public void OnTooltip(bool active)
    {
        if (active)
        {
            Tooltip.Show(description);
        }
        else
        {
            Tooltip.Hide();
        }
    }
    
}

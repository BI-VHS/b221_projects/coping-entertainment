using System;using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestStatus
{
    future, current, completed, failed
}

public enum SpecialQuestResult
{
    none, 
    finishTutorial, finishScientistMeet, fixTunnel, fixReactor
}


public class QuestManager : MonoBehaviour
{
    public static QuestManager instance;

    [SerializeField] private List<QuestData> questInput;

    private Dictionary<string, QuestData> quests;
    private List<QuestData> activeQuests;
    
    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    private void Start()
    {
        
        
        quests = new Dictionary<string, QuestData>();
        foreach (var quest in questInput)
        {
            var questInstance = Instantiate(quest);
            quests.Add(questInstance.questName, questInstance);
        }

        activeQuests = new List<QuestData>();
        
        instance = this;
    }

    /// <summary>
    /// Starts a new quest, if not already started. This will appear in the UI and potentially also in 
    /// </summary>
    public void StartQuest(string questName)
    {
        if (!quests.ContainsKey(questName))
        {
            Debug.LogAssertion("Trying to start unknown quest!");
            return;
        }
        
        QuestData quest = quests[questName];
        
        if (quest.questStatus != QuestStatus.future)
        {
            Debug.LogAssertion("Trying to start already started/done quest!");
            return;
        }
        
        quest.questStatus = QuestStatus.current;
        activeQuests.Add(quest);
        UpdateUI();
    }

    // finish quest
    public void EndQuest(string questName, bool successful)
    {
        if (!quests.ContainsKey(questName))
        {
            Debug.LogAssertion("Trying to end unknown quest!");
            return;
        }
        
        QuestData quest = quests[questName];
        
        if (quest.questStatus != QuestStatus.current)
        {
            Debug.LogAssertion("Trying to end quest which is done/not started!");
            return;
        }
        
        quest.questStatus = successful ? QuestStatus.completed : QuestStatus.failed;

        if (successful)
        {
            GameState.instance.GetQuestReward(0, quest.karmaGained, quest.ammoGained);
            
            Tooltip.Show($"Quest {quest.questName} complete!", 5f);

            if (quest.specialQuestResult != SpecialQuestResult.none)
            {
                ExecuteSpecialQuestEnd(quest.specialQuestResult);
            }
        }
        
        activeQuests.Remove(quest);
        UpdateUI();
    }

    // TODO actually do something
    private void ExecuteSpecialQuestEnd(SpecialQuestResult specialQuestResult)
    {
        Debug.Log($"Special quest finished: {specialQuestResult}.");
        switch (specialQuestResult)
        {
            case SpecialQuestResult.finishTutorial:
                WorldState.UpdateValue("tutorialDone");
                break;
            case SpecialQuestResult.finishScientistMeet:
                break;
            case SpecialQuestResult.fixTunnel:
                break;
            case SpecialQuestResult.fixReactor:
                break;
            case SpecialQuestResult.none:
            default:
                Debug.LogAssertion("Unknown quest ending!");
                break;
        }
    }

    public void SetQuestWaypoint(string questName)
    {
        SetQuestWaypoint(questName, quests[questName].currentWaypoint+1);
    }

    // move quest to new waypoint
    public void SetQuestWaypoint(string questName, int waypoint)
    {
        if (!quests.ContainsKey(questName))
        {
            Debug.LogAssertion("Trying to end unknown quest!");
            return;
        }
        
        QuestData quest = quests[questName];

        if (quest.questStatus != QuestStatus.current)
        {
            Debug.LogAssertion("Trying to add waypoint for non active quest!");
            return;
        }
        if (waypoint >= quest.totalWaypoints || waypoint < 0)
        {
            Debug.LogAssertion("Trying to set invalid waypoint id!");
            return;
        }

        if (waypoint < quest.currentWaypoint)
        {
            Debug.LogAssertion("trying to go to already complete quest phase!");
            return;
        }
        
        // waypoint is valid, set it
        quest.currentWaypoint = waypoint;
        
        Tooltip.Show($"New quest waypoint for {quest.questName} unlocked!\n", 3f);
        
        UpdateUI();
    }

    private void UpdateUI()
    {
        QuestView.instance.UpdateView(activeQuests);
    }

    public QuestData GetQuestData(string questName)
    {
        if (!quests.ContainsKey(questName))
        {
            Debug.LogAssertion("Quest not found!");
        }
        
        return quests[questName];
    }

}
    
    

using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum Scene
{
    menu,
    strelnice, karlak, tunel, andel
}

public enum HUDMode
{
    game, UI
}

public class UIManager : MonoBehaviour
{
    public GameObject MenuCanvas;
    public GameObject HUDCanvas;
    public GameObject LoadingScreenCanvas;

    public GameObject playerObject;

    /// <summary>
    /// True - controls move the player
    /// False - controls affect the UI
    /// </summary>
    public static HUDMode currentMode;
    
    public static UIManager instance;
    
    /// <summary>
    /// returns 0 iff no level is loaded, else currently loaded level number
    /// </summary>
    public int currentLevel { get; private set; }
    public bool inGame { get; private set; }

    /// <summary>
    /// Start is called before the first frame update
    /// </summary>
    private void Awake()
    {
        // dont show HUD canvas
        HUDCanvas.SetActive(false);
        LoadingScreenCanvas.SetActive(false);
        currentLevel = 0;
        inGame = false;

        currentMode = HUDMode.UI;
        
        Time.timeScale = 0f;
        
        // turn off player
        //playerObject.SetActive(false);

        instance = this;
    }

    /// <summary>
    /// 0 = go back to main menu = scene 0 (unload all other scenes = levels)
    /// n>0 = load nth level scene additively and unload all other level scenes
    /// TODO add scene to do the transition
    /// </summary>
    /// <param name="sceneNumber"></param>
    public void LoadScene(int sceneNumber)
    {
        Debug.Log($"Loading scene {sceneNumber} from scene {currentLevel}...");
        
        if (sceneNumber == 0)
        {
            LoadMenu();
        }
        else
        {
            if (sceneNumber == 1)
            {
                GameState.instance.UpdateUI();
            }
            
            LoadLevel(sceneNumber);
        }
        
        currentLevel = sceneNumber;
    }

    public void LoadNextLevel()
    {
        // TODO check if next level exists
        if (currentLevel >= SceneManager.sceneCountInBuildSettings - 1)
        {
            Debug.LogError("There is no next level!");
            return;
        }
        
        LoadScene(currentLevel + 1);
    }

    public void LoadPreviousLevel()
    {
        if (currentLevel == 1)
        {
            Debug.LogError("There is no previous level!");
            return;
        }
        
        LoadScene(currentLevel - 1);
    }

    // /// <summary>
    // /// Goes to a predetermined position at scene 1
    // /// </summary>
    // public void StartGame()
    // {
    //     LoadSceneAtPosition(1, new Vector3(75, 1, 0));
    // }

    /// <summary>
    /// load new scene and move player to given position
    /// </summary>
    public void LoadSceneAtPosition(int sceneNumber, Vector3 position)
    {
        // PlayerController.position = position;
        playerObject.transform.position = position;
        LoadLevel(sceneNumber);
    }

    private void LoadLevel(int sceneNumber)
    {
        // unload menu interface if loaded
        if (currentLevel == 0)
        {
            MenuCanvas.SetActive(false);
            LoadingScreenCanvas.SetActive(true);
            
            // dont unload mainScene
            SceneManager.LoadSceneAsync(sceneNumber, LoadSceneMode.Additive).completed += _ =>
            {
                LoadingScreenCanvas.SetActive(false);
                HUDCanvas.SetActive(true);
                Time.timeScale = 1f;

                currentMode = HUDMode.game;
                
                

                if (currentLevel == 1)
                {
                    QuestManager.instance.StartQuest("Weapons Hot");
                    PlayerController.instance.transform.position = new Vector3(82, 0.05f, 3);
                }
                
            };
            
        }
        
        // else unload previous(current) level, then load new one
        else
        {
            HUDCanvas.SetActive(false);
            LoadingScreenCanvas.SetActive(true);
            Time.timeScale = 0f;

            currentMode = HUDMode.UI;
            
            // menu already unloaded
            SceneManager.UnloadSceneAsync(currentLevel).completed += _ =>
            {
                SceneManager.LoadSceneAsync(sceneNumber, LoadSceneMode.Additive).completed += _ =>
                {
                    LoadingScreenCanvas.SetActive(false);
                    HUDCanvas.SetActive(true);
                    Time.timeScale = 1f;

                    currentMode = HUDMode.game;
                };
            };
        }
    }

    private void LoadMenu()
    {
        if (currentLevel == 0)
        {
            Debug.LogAssertion("Trying to load MainScene from MainScene. Aborting...", this);
            return;
        }

        currentMode = HUDMode.UI;
        
        HUDCanvas.SetActive(false);
        LoadingScreenCanvas.SetActive(true);
        Time.timeScale = 0f;
        
        // turn off player
        //playerObject.SetActive(false);
        // TODO save player data

        SceneManager.UnloadSceneAsync(currentLevel).completed += _ =>
        {
            LoadingScreenCanvas.SetActive(false);
            MenuCanvas.SetActive(true);
        };
    }

    public static string GetSceneName()
    {
        return instance.currentLevel switch
        {
            0 => "Main menu",
            1 => "Střelnice",
            2 => "Karlovo Náměstí",
            3 => "Tunel",
            4 => "Anděl",
            5 => "Řídící Středisko",
            _ => "Unknown scene"
        };
    }
    
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class AmmoTrigger : MonoBehaviour
{
    
    public bool isActive { get; private set; }

    private GameObject pressEtext;
    private GameObject ammoPhysical;

    // [SerializeField] private GameObject weaponSpawnPrefab;

    [SerializeField] [Tooltip("Druh munice")]
    private AmmoType ammoType;
    
    [SerializeField] [Range(0, 10000)] [Tooltip("")]
    private int ammoCount;

    
    [SerializeField] [Tooltip("none pro zbraň volně dostupnou, jinak vyber typ munice jako cenu")]
    private AmmoType priceType;
    
    /// <summary>
    /// 0 = freely accessible
    /// </summary>
    [SerializeField] [Range(0, 10000)] [Tooltip("")]
    private int price;

    private void Awake()
    {
        pressEtext = GetComponent<Transform>().GetChild(0).GetChild(0).gameObject;
        ammoPhysical = GetComponent<Transform>().parent.gameObject;
        
        SetVisibility(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (dist.magnitude > 5f)
        {
            Debug.Log("Trigger too far!");
            return;
        }
        
        SetVisibility(true);
        pressEtext.GetComponent<TextMeshProUGUI>().text = 
            price == 0 ? "Press E to take ammo" : "Press E to buy ammo";
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (!isActive && dist.magnitude <= 5f)
        {
            OnTriggerEnter(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    private void SetVisibility(bool v)
    {
        isActive = v;
        pressEtext.SetActive(v);
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public void OnWeaponInteractionComplete(InteractionResult interactionResult)
    {
        if (interactionResult == InteractionResult.positive)
        {
            GameState.instance.ChangeInAmmoCount(ammoType, ammoCount);
            GameState.instance.UpdateUI();
            
            if (price > 0)
            {
                GameState.instance.ChangeInAmmoCount(priceType, -1 * price);
                GameState.instance.UpdateUI();
            }
            
            // destroy item
            if (ammoPhysical != null)
            {
                Destroy(ammoPhysical);
                // also kills this script
            }
        }
    }
    
    private void Update()
    {
        if (isActive && Controls.GetButtonDownGame("interact"))
        {
            if (price == 0 || priceType == AmmoType.none)
            {
                // TODO just collect
                // InteractionManager.instance.Show(OnWeaponInteractionComplete, 
                //     GameState.instance.weapon, weaponData);
                
                OnWeaponInteractionComplete(InteractionResult.positive);
            }
            else
            {
                // TODO show window w / confirmation shit
                InteractionManager.instance.BuyItem(OnWeaponInteractionComplete,
                    $"__TODO__ ammo name", "__TODO__ ammo desc", priceType, price);
            }

            SetVisibility(false);
        }
    }

    /// <summary>
    /// Creates a new drop in the current scene
    /// </summary>
    public static void Instantiate(Vector3 position, AmmoType ammoType, int ammoCount, AmmoType priceAmmoType, int priceAmmoCount)
    {
        // todo better
        // TODO add to prefab folder
        GameObject wp = (GameObject) Instantiate(Resources.Load("AmmoSpawn"));
        Transform wpt = wp.transform;
        
        wpt.position = position;
        
        AmmoTrigger wpat = wpt.GetChild(wpt.childCount - 1).GetComponent<AmmoTrigger>();
        wpat.price = priceAmmoCount;
        wpat.priceType = priceAmmoType;
        wpat.ammoType = ammoType;
        wpat.ammoCount = ammoCount;

    }
    
    
}







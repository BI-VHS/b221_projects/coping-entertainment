using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoreTrigger : MonoBehaviour
{
    
    public bool isActive { get; private set; }

    public GameObject pressEtext;

    public string itemName;
    
    [TextArea(3, 10)]
    public string itemDescription;

    private void Awake()
    {
        SetVisibility(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (dist.magnitude > 5f)
        {
            // Debug.Log("Trigger too far!");
            return;
        }
        
        SetVisibility(true);
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (!isActive && dist.magnitude <= 5f)
        {
            OnTriggerEnter(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    private void SetVisibility(bool v)
    {
        isActive = v;
        pressEtext.SetActive(v);
    }
    
    private void Update()
    {
        if (isActive && Controls.GetButtonDownGame("interact"))
        {
            InteractionManager.instance.ShowLoreWrapped(itemName, itemDescription);

            SetVisibility(false);
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemWeaponTrigger : MonoBehaviour
{
    private static GameObject weaponPrefab;
    
    public bool isActive { get; private set; }

    private GameObject pressEtext;
    private GameObject weaponPhysical;

    // [SerializeField] private GameObject weaponSpawnPrefab;

    [SerializeField] [Tooltip("Která zbraň se spawnne")]
    private WeaponData weaponData;
    
    [SerializeField] [Tooltip("none pro zbraň volně dostupnou, jinak vyber typ munice jako cenu")]
    private AmmoType priceType;
    
    /// <summary>
    /// 0 = freely accessible
    /// </summary>
    [SerializeField] [Range(0, 10000)] [Tooltip("")]
    private int price;

    private void Awake()
    {
        pressEtext = GetComponent<Transform>().GetChild(0).GetChild(0).gameObject;
        weaponPhysical = GetComponent<Transform>().parent.gameObject;
        
        SetVisibility(false);

        weaponPrefab = (GameObject)Resources.Load("WeaponSpawn");
    }

    private void OnTriggerEnter(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (dist.magnitude > 5f)
        {
            Debug.Log("Trigger too far!");
            return;
        }
        
        SetVisibility(true);
        pressEtext.GetComponent<TextMeshProUGUI>().text = 
            WorldState.GetValue("weaponAllowed") == 0 ? "No permission to use weapon!" :
            price == 0 ? "Press E to take weapon" : "Press E to buy weapon";
    }

    private void OnTriggerStay(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (!isActive && dist.magnitude <= 5f)
        {
            OnTriggerEnter(other);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    private void SetVisibility(bool v)
    {
        isActive = v;
        pressEtext.SetActive(v);
    }

    // ReSharper disable Unity.PerformanceAnalysis
    public void OnWeaponInteractionComplete(InteractionResult interactionResult)
    {
        if (interactionResult == InteractionResult.positive)
        {
            
            // instantiate new weapon
            // TODO instantiate new weapon
            ItemWeaponTrigger.Instantiate(PlayerController.playerPosition, GameState.instance.weapon, AmmoType.none, 0);
            
            // update weapon
            GameState.instance.weapon = weaponData;
            

            if (price > 0)
            {
                GameState.instance.ChangeInAmmoCount(priceType, -1 * price);
                GameState.instance.UpdateUI();
            }
            
            // update counter
            WorldState.UpdateValue("weaponsTriedOut");
            if (WorldState.GetValue("weaponsTriedOut") >= 3)
            {
                QuestManager.instance.SetQuestWaypoint("Weapons Hot", 2);
            }
            
            // destroy item
            if (weaponPhysical != null)
            {
                Destroy(weaponPhysical);
                // also kills this script
            }
        }
    }
    
    private void Update()
    {
        if (isActive && Controls.GetButtonDownGame("interact"))
        {
            if (WorldState.GetValue("weaponsTriedOut") < 4)
            {
                return;
            }
            
            if (price == 0)
            {
                InteractionManager.instance.ShowWeapon(OnWeaponInteractionComplete, 
                    GameState.instance.weapon, weaponData);
            }
            else
            {
                InteractionManager.instance.BuyWeapon(OnWeaponInteractionComplete, 
                    GameState.instance.weapon, weaponData, priceType, price);
            }

            SetVisibility(false);
        }
    }

    /// <summary>
    /// Creates a new drop in the current scene
    /// </summary>
    public static void Instantiate(Vector3 position, WeaponData weaponData, AmmoType priceAmmoType, int priceAmmoCount)
    {
        // todo better
        GameObject wp = (GameObject) Instantiate(weaponPrefab);
        Transform wpt = wp.transform;

        wpt.position = position;

        ItemWeaponTrigger wpwt = wpt.GetChild(wpt.childCount - 1).GetComponent<ItemWeaponTrigger>();
        wpwt.price = priceAmmoCount;
        wpwt.priceType = priceAmmoType;
        wpwt.weaponData = weaponData;
    }
    
    
}







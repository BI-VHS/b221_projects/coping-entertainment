using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueTriggerBase : MonoBehaviour
{
    
    public bool isActive { get; private set; }

    public GameObject pressEtext;
    // public Transform canvasTransform;
    // public Transform parentTransform;

    public NPCGeneric NPC;
    
    public string NPCName;
    
    [TextArea(5, 20)]
    public string dialogue;
    [TextArea(3, 10)]
    public string replyNegative;
    [TextArea(3, 10)]
    public string replyPositive;

    protected void Awake()
    {
        SetVisibility(false);
    }

    protected void OnTriggerEnter(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (dist.magnitude > 5f)
        {
            Debug.Log("Trigger too far!");
            return;
        }
        
        SetVisibility(true);
    }

    protected void OnTriggerStay(Collider other)
    {
        Vector3 dist = other.transform.position - PlayerController.instance.rigidBody.position;
        if (!isActive && dist.magnitude <= 5f)
        {
            OnTriggerEnter(other);
        }
    }

    protected void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    protected void SetVisibility(bool v)
    {
        isActive = v;
        pressEtext.SetActive(v);
    }

    public virtual void Positive()
    {
        
    }

    public virtual void Negative()
    {
        
    }

    // ReSharper disable Unity.PerformanceAnalysis
    protected void OnInteractionComplete(InteractionResult interactionResult)
    {
        if (interactionResult == InteractionResult.positive)
        {
            Positive();
        }
        else if (interactionResult == InteractionResult.negative)
        {
            Negative();
        }
    }

    public virtual void Interact()
    {
        InteractionManager.instance.ShowDialogue(NPCName, dialogue, replyNegative, replyPositive, OnInteractionComplete);
    }
    
    protected void Update()
    {
        if (isActive && Controls.GetButtonDownGame("interact"))
        {
            Interact();
            SetVisibility(false);
        }
        
        UpdateInherited();
    }

    
    /// <summary>
    /// Deprecated, do not use
    /// TODO move door to own object
    /// </summary>
    protected virtual void UpdateInherited(){}
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPC_NVM_loop : MonoBehaviour
{
    NavMeshAgent agent;
    public Transform[] waypoints;
    int waypointIndex;
    Vector3 target;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        UpdateDestination();
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, target) < 1) {

            StartCoroutine(Waiter());
            IterateWaypointIndex();
            UpdateDestination();

        }
    }
    void UpdateDestination() {
        target = waypoints[waypointIndex].position;
        agent.SetDestination(target);
    }

    void IterateWaypointIndex() {
        waypointIndex++;
        if (waypointIndex == waypoints.Length) {
            waypointIndex = 0;
        }
    }
    IEnumerator Waiter() {
        
        gameObject.GetComponent<NavMeshAgent>().isStopped = true;
        yield return new WaitForSecondsRealtime(2);
        gameObject.GetComponent<NavMeshAgent>().isStopped = false;
        
    }
}

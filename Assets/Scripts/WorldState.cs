using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// Current variables:
/// weaponsTriedOut
/// tutorialDone
/// </summary>
public class WorldState : MonoBehaviour
{
    
   
    
    
    // /// <summary>
    // /// True if player got permission from Recepcni1 to talk with the manager in strelnice
    // /// </summary>
    // public static bool hasPermissionFromRecepcni = false;
    //
    // /// <summary>
    // /// True if the gate in strlnice has been sucessfully opened
    // /// </summary>
    // public static bool gateOpened = false;
    //
    // /// <summary>
    // /// True after talking to the manager in strelncie
    // /// </summary>
    // public static bool canUseWeapons = false;
    //
    // /// <summary>
    // /// Increments each time the player selects a new weapon.
    // /// </summary>
    // public static int weaponsTriedOut = 0;
    //
    // /// <summary>
    // /// True if tutorial quest done. Then the player can leave strelnice
    // /// </summary>
    // public static bool tutorialDone = false;

    public delegate void ValueUpdate(int newValue);

    // private static event ValueUpdate[] valueUpdates = new ValueUpdate[10];
    
    private static Dictionary<string, WorldStateVariable> state = new();

    /// <summary>
    /// Creates a new world state variable initialized to 0 (false).
    /// Creates new Event.
    /// </summary>
    public static void InitializeValue(string name)
    {
        WorldStateVariable newValue = new();
        state.Add(name, newValue);
    }

    /// <summary>
    /// Updates value of variable by incrementing it
    /// Sends event to all subscribed.
    /// </summary>
    public static void UpdateValue(string name)
    {
        if (!state.ContainsKey(name))
        {
            InitializeValue(name);
        }
        
        state[name]._value++;
        state[name].Invoke();
    }

    /// <summary>
    /// Returns event to subscribe to.
    /// </summary>
    public static void SubscribeValueEvent(string name, ValueUpdate stuffToDo)
    {
        if (!state.ContainsKey(name))
        {
            InitializeValue(name);
        }
        
        state[name]._event += stuffToDo; 
    }
    
    public static int GetValue(string name)
    {
        if (!state.ContainsKey(name))
        {
            InitializeValue(name);
        }
        
        return state[name]._value; 
    }
}

public class WorldStateVariable
{
    public int _value;
    public event WorldState.ValueUpdate _event;

    public void Invoke()
    {
        _event?.Invoke(_value);
    }

    public WorldStateVariable()
    {
        _value = 0;
    }
}

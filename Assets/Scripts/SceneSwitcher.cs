using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// Script attached to scene teleport trigger
/// Changes the active scene and teleports player to given coordinates
/// </summary>
public class SceneSwitcher : MonoBehaviour
{
    public bool isActive { get; private set; }

    public GameObject pressEtext;
    private TextMeshProUGUI pet;

    public int sceneID;
    public Vector3 sceneCoords;

    private void Awake()
    {
        pet = pressEtext.GetComponent<TextMeshProUGUI>();
        SetVisibility(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        SetVisibility(true);
        pet.text = WorldState.GetValue("weaponsTriedOut") >= 3 ? 
            "Press E to exit" : "Finish the quest to exit";
    }

    private void OnTriggerExit(Collider other)
    {
        SetVisibility(false);
    }

    private void SetVisibility(bool v)
    {
        isActive = v;
        pressEtext.SetActive(v);
    }

    private void Update()
    {
        if (isActive && Controls.GetButtonDownGame("interact"))
        {
            if (WorldState.GetValue("tutorialDone") > 0)
            {
                if (WorldState.GetValue("weaponsTriedOut") >= 3)
                {
                    QuestManager.instance.EndQuest("Weapons Hot", true);
                }
                return;
            }
            
            // todo check if new scene is main menu tho
            UIManager.instance.LoadSceneAtPosition(sceneID, sceneCoords);
            
            // just to be sure
            Tooltip.Hide();

            SetVisibility(false);
        }
    }
}

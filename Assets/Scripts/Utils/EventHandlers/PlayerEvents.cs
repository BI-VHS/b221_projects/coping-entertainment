using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEvents : MonoBehaviour {
    public static PlayerEvents current;

    public GameObject bullet;
    public GameObject shootPoint;
    private void Awake() {
        current = this;
    }

    // public event Action onMoveUpEvent;
    // public event Action onMoveDownEvent;
    // public event Action onMoveLeftEvent;
    // public event Action onMoveRightEvent;
    public event Action onPrimaryEvent;
    public event Action onSecondaryEvent;
    public event Action onPrevWeaponEvent;
    public event Action onNextWeaponEvent;
    public event Action onUseEvent;
    public event Action shooting;

    //
    // public void MoveUpEvent () {
    //     onMoveUpEvent?.Invoke();
    // }
    //
    // public void MoveDownEvent () {
    //     onMoveDownEvent?.Invoke();
    // }
    //
    // public void MoveLeftEvent () {
    //     onMoveLeftEvent?.Invoke();
    // }
    //
    // public void MoveRightEvent () {
    //     onMoveRightEvent?.Invoke();
    // }

    public void Shooted() {
        shooting?.Invoke();
    }
    public void PrimaryEvent () {
        onPrimaryEvent?.Invoke();
    }

    public void SecondaryEvent () {
        onSecondaryEvent?.Invoke();
    }

    public void PrevWeaponEvent () {
        onPrevWeaponEvent?.Invoke();
    }

    public void NextWeaponEvent () {
        onNextWeaponEvent?.Invoke();
    }

    public void UseEvent () {
        onUseEvent?.Invoke();
    }

}

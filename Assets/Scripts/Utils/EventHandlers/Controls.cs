using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour {
	private bool m_ctrlPressed;
	private bool m_shiftPressed;

	public Camera mainCamera;
	public LayerMask ignoreLayers;
	public static Controls instance;

	Gun gun;
	
	private void Start()
	{
		instance = this;
	}

	public void Update() {
		HandleKeyboardInput();
		HandleMouseInput();
	}

	public static bool GetButtonDownUI(string button)
	{
		return UIManager.currentMode == HUDMode.UI && Input.GetButtonDown(button);
	}

	public static bool GetButtonDownGame(string button)
	{
		return UIManager.currentMode == HUDMode.game && Input.GetButtonDown(button);
	}

	public static bool GetButtonUI(string button)
	{
		return UIManager.currentMode == HUDMode.UI && Input.GetButton(button);
	}

	public static bool GetButtonGame(string button)
	{
		return UIManager.currentMode == HUDMode.game && Input.GetButton(button);
	}

	public static bool GetButtonUpUI(string button)
	{
		return UIManager.currentMode == HUDMode.UI && Input.GetButtonUp(button);
	}

	public static bool GetButtonUpGame(string button)
	{
		return UIManager.currentMode == HUDMode.game && Input.GetButtonUp(button);
	}

	public static Vector3 GetMovementGame()
	{
		return UIManager.currentMode == HUDMode.game ? 
			new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")) : Vector3.zero;
	}

	private void HandleKeyboardInput() {

		m_ctrlPressed = Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl);
		m_shiftPressed = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

		// if (Input.GetKey(KeyCode.W)) {
		// 	PlayerEvents.current.MoveUpEvent();
		// }
		// if (Input.GetKey(KeyCode.S)) {
		// 	PlayerEvents.current.MoveDownEvent();
		// }
		// if (Input.GetKey(KeyCode.A)) {
		// 	PlayerEvents.current.MoveLeftEvent();
		// }
		// if (Input.GetKey(KeyCode.D)) {
		// 	PlayerEvents.current.MoveRightEvent();
		// }

	}

	private void HandleMouseInput() {
		if (Input.GetMouseButtonDown(0)) {
			//PlayerEvents.current.PrimaryEvent();
			PlayerEvents.current.Shooted();
		}
	}

	public static Vector3 GetWorldMousePos() {
		Ray ray = instance.mainCamera.ScreenPointToRay(Input.mousePosition);
		Vector3 pos = Vector3.zero;
		if (Physics.Raycast(ray, out RaycastHit raycastHit, 10000f, ~instance.ignoreLayers)) {
			pos = raycastHit.point;
			pos.y += 0.1f;
		}

		return pos;
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
    
    private void Update() {
        var player_pos = transform.parent.position;
        var mouse_lookat = Controls.GetWorldMousePos();
        var mouse_pos = Input.mousePosition;

        var x = player_pos.x;//Mathf.Lerp(player_pos.x, mouse_lookat.x, 0.1f);
        var y = player_pos.y;
        var z = player_pos.z;//Mathf.Lerp(player_pos.z, mouse_lookat.z, 0.1f);
        
        // var x = (player_pos.x + mouse_lookat.x) / 2;
        // var y = (player_pos.y + mouse_lookat.y) / 2;
        // var z = (player_pos.z + mouse_lookat.z) / 2;

        transform.position = new Vector3(x, y, z);
    }

}

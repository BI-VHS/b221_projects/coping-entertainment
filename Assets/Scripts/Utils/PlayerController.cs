using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
	public float movementSpeed;
	public float movementTime;

	private const float movementCoef = 10000f;

	public Camera mainCamera;
	public Rigidbody rigidBody;

	public static Vector3 playerPosition => instance.rigidBody.position;

	public Vector3 direction { get; private set; }
	
	public static PlayerController instance;

	private void Start() {
		// PlayerEvents.current.onMoveUpEvent += OnMoveUp;
		// PlayerEvents.current.onMoveDownEvent += OnMoveDown;
		// PlayerEvents.current.onMoveLeftEvent += OnMoveLeft;
		// PlayerEvents.current.onMoveRightEvent += OnMoveRight;
		PlayerEvents.current.onPrimaryEvent += OnPrimary;

		instance = this;
	}

	private void OnDestroy() {
		// PlayerEvents.current.onMoveUpEvent -= OnMoveUp;
		// PlayerEvents.current.onMoveDownEvent -= OnMoveDown;
		// PlayerEvents.current.onMoveLeftEvent -= OnMoveLeft;
		// PlayerEvents.current.onMoveRightEvent -= OnMoveRight;
		PlayerEvents.current.onPrimaryEvent -= OnPrimary;
	}

	public void Update()
	{
		direction = Controls.GetMovementGame();
	
		
		rigidBody.AddForce(direction.normalized * (movementCoef * Time.deltaTime), ForceMode.Force);

		var childPlayer = GameObject.FindGameObjectWithTag("Player");
		var gun = GameObject.FindGameObjectWithTag("GUN");
		var mousePos = Controls.GetWorldMousePos();
		
		var lookPos = mousePos - childPlayer.transform.position;

		var gunpos = mousePos - gun.transform.position;
		
		lookPos.y = 0;
		var rotation = Quaternion.LookRotation(lookPos);
		childPlayer.transform.rotation = rotation;


		if (MathF.Abs(gunpos.x) > 1) {
			var gun_rotation = Quaternion.LookRotation(gunpos);
			gun.transform.rotation = gun_rotation;
		}
		




	}
	// private void OnMoveUp() {
	// 	direction += transform.forward * movementSpeed;
	// }
	//
	// private void OnMoveDown(){
	// 	direction -= transform.forward * movementSpeed;
	// }
	//
	// private void OnMoveLeft(){
	// 	direction -= transform.right * movementSpeed;
	// }
	//
	// private void OnMoveRight(){
	// 	direction += transform.right * movementSpeed;
	// }

	public Vector3 getRotation() {

		var childPlayer = GameObject.FindGameObjectWithTag("Player");
		var mousePos = Controls.GetWorldMousePos();
		var lookPos = mousePos - childPlayer.transform.position;
		return lookPos;
	}

	private void OnPrimary(){

	}

	/// <summary>
	/// Returns distance from player transform
	/// </summary>
	/// <param name="other"></param>
	/// <returns></returns>
	public static float Distance(Vector3 other)
	{
		return Vector3.Distance(playerPosition, other);
	}

}

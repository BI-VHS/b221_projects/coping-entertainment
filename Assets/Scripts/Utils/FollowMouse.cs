using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {
    public LineRenderer lr;

    private void Start() {
        lr.positionCount = 2;
    }

    void Update() {
        if (UIManager.currentMode == HUDMode.UI)
        {
            return;
        }
        
        var pos = Controls.GetWorldMousePos();
        var _transform = transform;
        _transform.position = pos;
        lr.SetPosition(0, _transform.parent.position);
        lr.SetPosition(1, pos);
    }
}

using UnityEngine;

namespace NPCv2.Strelnice
{
    public class RandomShootingAtTarget : NPCState
    {
        [SerializeField] 
        private Transform target;
        
        protected override void StartIdle()
        {
            if (Random.value < 0.5f)
            {
                EnqueueNewAction(new AIState(Activity.waiting, 5, false));

                if (Random.value < 0.84f)
                {
                    float trefil = Mathf.Min(RNG.k(4, 4), 12f) / 12;
                    npcFunc.Say($"Trefila jsem {trefil:P2}!");
                }
                else
                {
                    npcFunc.Say("Trefa do černého");
                }
            }
            else
            {
                EnqueueNewAction(new AIState(Activity.shooting, RNG.k(2, 2), false)
                {
                    goalType = GoalType.nonplayer,
                    goalVector = target.position
                });
                EnqueueNewAction(new AIState(Activity.reloading, 3, false));
            }
        }
    }
}

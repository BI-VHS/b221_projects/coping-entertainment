using System.Collections;
using System.Collections.Generic;
using NPCv2;
using UnityEngine;

public class Skladnik : NPCState
{
    public Transform ammoStash;
    public Transform table;
    
    protected override void OnStart()
    {
        base.OnStart();

        WorldState.SubscribeValueEvent("ammoRequest", value =>
        {
            EnqueueNewAction(new AIState(Activity.travelling, float.PositiveInfinity, true)
            {
                goalType = GoalType.nonplayer,
                goalVector = ammoStash.position
            });
            EnqueueNewAction(new AIState(Activity.travelling, 3, false)
            {
                goalType = GoalType.nonplayer,
                goalVector = table.position,
                callback = (() =>
                {
                    StartIdle();
                })
            });
        });
    }

    protected override void StartIdle()
    {
        var position = table.position + Vector3.up;
        AmmoTrigger.Instantiate(position, AmmoType.mm556, 10, 0, 0);
        AmmoTrigger.Instantiate(position, AmmoType.mm762, 5, 0, 0);
        AmmoTrigger.Instantiate(position, AmmoType.mm858, 2, 0, 0);
        
        base.StartIdle();
    }
}

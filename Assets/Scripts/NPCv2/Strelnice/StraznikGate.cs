using UnityEngine;

namespace NPCv2.Strelnice
{
    public class StraznikGate : NPCState
    {
        [SerializeField] 
        private GameObject gate;
        
        protected override void OnStart()
        {
            base.OnStart();

            WorldState.SubscribeValueEvent("openGate", value =>
            {
                Vector3 pos = gate.transform.position;
                pos.z = -7f;
                gate.transform.position = pos;

            });
        }
    }
}

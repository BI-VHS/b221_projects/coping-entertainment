using System;
using System.Collections;
using System.Collections.Generic;
using NPCv2;
using TMPro;
using UnityEngine;

/// <summary>
/// "view" script
/// Covers: npc actions (say, fire, reload, etc.)
/// Also trigger actions
/// </summary>
public class NPCFunctionality : MonoBehaviour
{
    private Transform npcTransform;
    private NPCState npcState;
    private GameObject interactionText;
    private TextMeshProUGUI sayText;

    private Transform shootPos;
    
    private bool triggerTextActive;

    private const float maxTriggerDistance = 5f;

    private float sayTime;
    private Queue<string> sayQueue;

    private void Awake()
    {
        npcTransform = GetComponent<Transform>().parent;
        
        npcState = GetComponent<NPCState>();
        interactionText = transform.GetChild(0).GetChild(0).gameObject;
        sayText = transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>();

        triggerTextActive = true;
        
        SetInteractorVisibility(false);

        sayTime = 0;
        sayQueue = new Queue<string>();
        Say("");
        
        
    }

    private void Start()
    {
        shootPos = GetComponent<NPCState>().shootPoint.transform;
    }

    public float PlayerDistanceFromNPC()
    {
        Vector3 dist = npcTransform.position - PlayerController.playerPosition;
        return dist.magnitude;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Vector3 dist = other.transform.position - PlayerController.playerPosition;
        // if (dist.magnitude > maxTriggerDistance)
        if (PlayerDistanceFromNPC() > maxTriggerDistance)
        {
            // Debug.Log("Trigger too far!");
            return;
        }

        if (!npcState.wantsToTalk)
        {
            Debug.Log("NPC doesn't want to chat.");
            return;
        }
        
        SetInteractorVisibility(true);
    }

    private void OnTriggerStay(Collider other)
    {
        SetInteractorVisibility(npcState.wantsToTalk);
        
        if (triggerTextActive) 
            return;
        
        // Vector3 dist = other.transform.position - PlayerController.playerPosition;
        // if (dist.magnitude > maxTriggerDistance)
        if (PlayerDistanceFromNPC() > maxTriggerDistance)
        {
            return;
        }

        if (!npcState.wantsToTalk)
        {
            return;
        }
        
        OnTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        SetInteractorVisibility(false);
    }

    private void SetInteractorVisibility(bool isVisible)
    {
        if (triggerTextActive == isVisible)
        {
            return;
        }
        
        triggerTextActive = isVisible;
        interactionText.SetActive(isVisible);
    }

    private void OnInteract()
    {
        // add Dialogue to action queue. IDK about parameters tho gl
        npcState.EnqueueNewAction(AIState.dialogue);
    }

    public void Say(string sentence)
    {
        sayQueue.Enqueue(sentence);
    }

    private void Update()
    {
        UpdateInteraction();
        UpdateSay();
    }

    private void UpdateSay()
    {
        if (sayTime > 0.75f)
        {
            sayTime -= Time.deltaTime;
            if (sayTime <= 0.75f)
            {
                sayText.text = "";
                return;
            }
        }

        // if (!(sayTime >= 0)) return;
        
        sayTime -= Time.deltaTime;

        if (!(sayTime < 0)) return;

        sayText.text = "";
            
        if (sayQueue.Count <= 0) return;
                
        sayText.text = sayQueue.Dequeue();
        sayTime = (float)sayText.text.Length / 10 + 2f;

    }

    //TODO make cursor not go farther than 5m and delete ontriggerstay reappearing
    private void UpdateInteraction()
    {
        if (!triggerTextActive) 
            return;
        
        if (!Controls.GetButtonDownGame("interact")) 
            return;

        if (!npcState.wantsToTalk)
        {
            return;
        }
        
        OnInteract();
        SetInteractorVisibility(false);
    }

    /// <summary>
    /// True if NPC can see player
    /// </summary>
    public bool CanSeePlayer()
    {
        var rayDirection = PlayerController.playerPosition - transform.position;
        if (Physics.Raycast (shootPos.position, rayDirection, out var hit))
        {
            return hit.transform.name.Equals("Player");
        }

        return false;
    }

    /// <summary>
    /// TODO random
    /// </summary>
    public float GenerateSpeechDelay(float chattiness)
    {
        // 0 -> 50k seconds delay
        // 1 -> 5 seconds delay
        return 5 / (chattiness+0.0001f);
    }
    
}

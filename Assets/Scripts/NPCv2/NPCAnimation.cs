using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Contains handles which are called upon NPC activities
/// </summary>
public class NPCAnimation : MonoBehaviour
{
    private NPCFunctionality npcf;

    private void Awake()
    {
        npcf = GetComponent<NPCFunctionality>();
    }

    /// <summary>
    /// Započne idle animaci.
    /// </summary>
    public void OnWaitingStart()
    {
        // npcf.Say("Waiting...");
    }

    /// <summary>
    /// Ukončí idle animaci
    /// </summary>
    public void OnWaitingEnd()
    {
        // npcf.Say("Finished Waiting...");
    }

    /// <summary>
    /// Započne animaci chůze.
    /// </summary>
    public void OnTravelStart()
    {
        // npcf.Say("Going to pos...");
    }

    /// <summary>
    /// Ukončí animaci chůze.
    /// </summary>
    public void OnTravelEnd()
    {
        // npcf.Say("At pos...");
    }

    /// <summary>
    /// Začne držet zbraň.
    /// </summary>
    public void OnHoldGun()
    {
        // npcf.Say("Holding gun...");
    }

    /// <summary>
    /// Schová zbraň (typicky po přebití).
    /// </summary>
    public void OnHideGun()
    {
        // npcf.Say("Hiding Gun...");
    }

    /// <summary>
    /// NPC vystřelí. Vždy voláno několikkrát mezi OnHoldGun a OnHideGun.
    /// Bude již ve správné pozici a rotaci.
    /// </summary>
    public void OnFire()
    {
        // npcf.Say("Pew pew");
    }

    /// <summary>
    /// Přebíjení zbraně s určenou délkou.
    /// Stačí jedna krátká animace pro všechny přebíjení, např. schování zbraně a následné zvednutí zbraně.
    /// Není potřeba dělat úměrné délce nabíjení.
    /// </summary>
    public void OnReload()
    {
        // npcf.Say("Reloading");
    }

    /// <summary>
    /// Animace švihnutí rukou, jednorázová.
    /// </summary>
    public void OnMelee()
    {
        // npcf.Say("*hits you*");
    }
    
    /// <summary>
    /// Započne animaci spaní.
    /// Stačí jen položit NPC do stavu vleže. Pozice už bude správně nastavená.
    /// </summary>
    public void OnSleepStart()
    {
        // npcf.Say("Started sleeping...");
    }

    /// <summary>
    /// Ukončí animaci spaní.
    /// </summary>
    public void OnSleepEnd()
    {
        // npcf.Say("Sleep end");
    }

    /// <summary>
    /// Animace smrti. Stačí něco jako v Minecraftu, prostě smrt. Trvá cca 3 vteřiny.
    /// NPC pak zmizí úplně, zbyde jen loot.
    /// </summary>
    public void OnDeathStart()
    {
        // npcf.Say("I just died!");
    }

}

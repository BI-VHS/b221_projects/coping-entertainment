using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// values = priority values
/// lower = higher priority
/// will have to 
/// </summary>

public enum Activity
{
    // 0 - 9: execute immediately
    death = 1,
    // melee and shooting should not be concurrent
    
    // below 50: high priority
    melee = 10,
    shooting = 11,
    reloading = 12,
    travelling = 20,
    aggressive = 30,
    
    // above 50: low priority
    dialogue = 70,
    sleeping = 80,
    waiting = 90,
    // idle will only be enqueued when queue empty
    idle = 100, 
    
}

public enum GoalType
{
    player, nonplayer
}

/// <summary>
/// Base class for action queue
/// </summary>


[System.Serializable] 
public class AIState
{
    public delegate void OnCompleted();

    public Activity activity;
    
    
    public float activityTime { get; set; }
    public bool immediatePriority { get; private set; }

    /// <summary>
    /// player - goal is the player transform
    /// nonplayer - goal is a static vector
    /// </summary>

    public OnCompleted callback = null;

    public GoalType goalType;
    public Vector3 goalVector;

    public Vector3 currentGoal => goalType == GoalType.player ? PlayerController.playerPosition : goalVector;

    public static AIState idling => new AIState(Activity.idle, float.PositiveInfinity, false);
    public static AIState dialogue => new AIState(Activity.dialogue, 0.0001f, true);

    public static AIState aggroed => new AIState(Activity.aggressive, 0f, false);

    public static AIState death => new AIState(Activity.death, 3f, true);

    public AIState(Activity activity, float activityTime, bool immediate)
    {
        this.activity = activity;
        this.activityTime = activityTime;
        this.immediatePriority = immediate;
    }
}

public class Priotitizer : IComparer<AIState>
{
    public int Compare(AIState x, AIState y)
    {
        if (ReferenceEquals(x, y)) return 0;
        if (ReferenceEquals(null, y)) return 1;
        if (ReferenceEquals(null, x)) return -1;

        return x.activity.CompareTo(y.activity);
        
    }
}

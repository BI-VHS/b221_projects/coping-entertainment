using System;
using PriorityQueue.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace NPCv2
{
    /// <summary>
    /// Organizes NPC activity
    /// NPC does the following automatically, based on preset values:
    /// wait, sleep, attack/calm down, go somewhere, dialogue, death
    /// NPC can be modified by the following code:
    /// - what it does when it has nothing to do
    /// - what actions it does periodically (e.g. 19h -> go to bed and sleep)
    /// </summary>
    public class NPCState : MonoBehaviour
    {
        protected NPCDialogue npcDialogue;
        protected NPCFunctionality npcFunc;
        protected NPCAnimation npcAnimation;
        protected Transform npcTransform;
        protected NavMeshAgent npcNavigation;
        protected Animator npcAnimator;

        [SerializeField] 
        public GameObject shootPoint;

        protected PriorityQueue<AIState> actionQueue;
    

        [SerializeField] 
        public AIState currentState;
    

        [SerializeField] 
        public AIState initState = AIState.idling;
    

        /// <summary>
        /// The current state of the NPC
        /// </summary>
        public Activity currentActivity => currentState.activity;

        /// <summary>
        /// How much time the current activity has left
        /// </summary>
        public float currentActivityTimer
        {
            get => currentState.activityTime;
            set => currentState.activityTime = value;
        }

        /// <summary>
        /// True if currently aggressive
        /// </summary>
        public bool isAggroed { get; protected set; }

        protected int _health;
        protected int _maxhealth;
    
        /// <summary>
        /// True if currently armed
        /// </summary>
        protected bool isArmed => weaponData != null;

        public bool wantsToTalk => 
            currentActivity is Activity.idle or Activity.waiting;

        private Vector3 lastNpcPosition;
        public float speed;

        #region ExternalStateVariables

        [Header("Initial state")]

        // [Tooltip("Co dělá defaultně - většinou idle(npc) nebo sleeping(zombie)")]

        [SerializeField] 
        protected int deafultHealth = 100;
    
        [SerializeField] 
        protected int deafultMaxHealth = 100;
    
        public int health
        {
            get => _health;
            set
            {
                int minHealth = immortal ? 1 : 0;
                _health = Mathf.Clamp(value, minHealth, maxHealth);
            }
        }

        public int maxHealth
        {
            get => _maxhealth;
            protected set
            {
                float ratio = (float)value / _maxhealth;
                _maxhealth = value;
                _health = (int) Math.Round(_health * ratio);
            }
        }

        [Tooltip("true = nemůže být zabit (např. je důležitý v příběhu)")] [SerializeField] 
        protected bool immortal = false;

        protected bool dead;

        [Header("Waiting")]
        [Tooltip("Hlášky, když nic nedělá")] [SerializeField] [TextArea]
        protected string[] idleSpeech;
    
        [Tooltip("0... nikdy nic neřekne, 1... mluví dost často")] [Range(0, 1)] [SerializeField] 
        protected float idleChattiness = 0.5f;

        protected float timeUntilSay;
    
        [Header("Aggressive")] 
    
        [Tooltip("Hlášky, když je naštvaný")] [SerializeField] [TextArea]
        protected string[] aggressiveSpeech;
    
        [Tooltip("0... nikdy nic neřekne, 1... mluví dost často")] [Range(0, 1)] [SerializeField] 
        protected float aggressiveChattiness = 0.5f;
    
        [Tooltip("Jak moc se hráč musí přiblížit, aby se automaticky naštval. -1 = nikdy")] [Range(-1, 100)] [SerializeField] 
        protected float aggravateThreshold = -1;
    
        [FormerlySerializedAs("CalmThreshold")] 
        [Tooltip("Jak moc se hráč musí oddálit, aby se automaticky uklidnil. -1 = nikdy")] [Range(-1, 100)] [SerializeField] 
        protected float calmThreshold = -1;

        [Tooltip("True = má zbraň a používá ji. False = nemá zbraň nebo ji nepoužívá.")] [SerializeField]
        protected bool usesWeapon = false;
    
        // DONT USE
        //protected Transform hostileTarget;
    
        [Header("Sleeping")]
        [Tooltip("Jeho postel")] [FormerlySerializedAs("bedPosition")] [SerializeField] 
        protected Transform bed;

        [Tooltip("Kde si ráno stoupne před postel")] [SerializeField] 
        protected Vector3 inFrontOfBedPosition;

        protected Vector3 workPosition;

        [Tooltip("false = nikdy nepůjde spát")] [SerializeField]
        protected bool goesToSleep = false;
    
        [Tooltip("Kdy spí")] [Range(0, 23)] [SerializeField]
        protected int sleepTime = 19;

        [Tooltip("Kdy vstává")] [Range(0, 23)] [SerializeField] 
        protected int awakeTime = 5;

        [Tooltip("Jak blízko musí být hráč, aby se NPC probudilo. NPC se bude chovat, jako kdyby bylo ráno.")] [SerializeField]
        protected float wakeUpDistance = 2f;

        [Header("Travelling")] 
        [Tooltip("Jak moc mluví, když někam jde. 0... nikdy nic neřekne, 1... mluví dost často")] [SerializeField]
        [Range(0,1)]
        protected float travelChattiness = 0.25f;

        [Tooltip("Jak rychle chodí")]
        [Range(0.1f,10)] [SerializeField]
        protected float walkingSpeed = 3.5f;

        protected float goalDist => (currentState.currentGoal - npcTransform.position).magnitude;

        // protected bool stopUponTargetVisible; // TODO
        // protected bool meleeUponCloseEnough;

        protected const float maxDistToEndTravel = 1.33f;
    
        [Header("Melee")] 
        [Tooltip("Když je moc daleko, tak útok nedudělá damage")] [SerializeField]
        protected float meleeMaxDistance = 1.5f;

        [Tooltip("Poškození při melee útoku")] [Range(1,100)] [SerializeField]
        protected int meleeDamage = 12;

        [Header("Shooting")] 

        [Tooltip("Sem vložte ScriptableObject jeho zbraně, pokud nějakou má.")] [SerializeField] 
        protected WeaponData weaponData;
    
        [Tooltip("0 - pokaždé vystřelí celý zásobník. > 0: vystřelí náhodný menší počet střel než nabije.")] [Range(0, 1)] [SerializeField]
        protected float salvoCountVariation = 0;
        
        [Tooltip("Jak moc se otáčí mezi výstřely. 0 - všechny výstřely trefí do stejného místa. 100 - vždy trefí hráče")] [Range(0, 100)] [SerializeField]
        protected int aimTurnSpeed = 20;

        // DONT USE! always shoot player lol
        //protected Transform shootingTarget;
    
        [Header("Reloading")]
        [Tooltip("1... nabíjí stejně dlouho jak hráč. >1 - nabíjí déle, <1 - méně dlouho")] [Range(0, 5)] [SerializeField]
        protected float reloadCoef = 1;

        protected float reloadTimeLeft;
    
        // dialogue header
    
        // death header
        // TODO some drop loot
        [Header("Death")]
        [Tooltip("Šance na drop své zbraně po smrti, pokud nějakou má.")] [Range(0, 1)] [SerializeField]
        protected float dropWeaponChance = 0;

        [Tooltip("Typ munice, který se má dropovat.")] [SerializeField]
        protected AmmoType dropAmmoType = AmmoType.mm556;
    
        [Tooltip("Množství munice, které dropne po smrti bude od 0 do dvounásobku se zajímavou distribucí")] [Range(0, 100)] [SerializeField]
        protected int dropAmmoQuantity = 5;

        #endregion

        #region common methods
    
        // Start is called before the first frame update
        private void Start()
        {
            npcDialogue = GetComponent<NPCDialogue>();
            npcFunc = GetComponent<NPCFunctionality>();
            npcAnimation = GetComponent<NPCAnimation>();
            npcTransform = GetComponent<Transform>().parent;
            npcNavigation = GetComponent<Transform>().parent.gameObject.GetComponent<NavMeshAgent>();
            
            npcAnimator = GetComponent<Transform>().parent.gameObject.GetComponent<Animator>();


            actionQueue = new PriorityQueue<AIState>(new Priotitizer());

            maxHealth = deafultMaxHealth;
            health = deafultHealth;

            dead = false;

            isAggroed = false;
            
            // return to original position
            workPosition = npcTransform.position;

            // TODO -=
            WorldTime.OnNewHour += HourlyUpdate;
        
            OnStart();

        }

        /// <summary>
        /// Add new action to end of queue
        /// TODO some actions go priotity
        /// </summary>
        public void EnqueueNewAction(AIState state)
        {
            // demo version: adds to end of list regardless of what it is
            // things like Death or Dialogue should not wait until walking finished
            actionQueue.Offer(state);

            // if currently nothing to do, switch
            if (currentState == null || currentActivity == Activity.idle)
            {
                FetchNewAction();
            }

            // if going to important state, do that immediately
            else if (state.immediatePriority)
            {
                EnqueueNewAction(currentState); // do this later, if dialogue for example
                FetchNewAction(); // do the current one NOW
            }
        
            // does NOT fetch
            // fetch is done later automatically
        }

        /// <summary>
        /// Called after task done or when beginning
        /// do NOT call when normal action enqueued
        /// Takes first action from queue and starts it
        /// No new action in queue -> go to idle
        /// </summary>
        protected void FetchNewAction()
        {
            // some task is queued
            // move to next task
            if (actionQueue.Count > 0)
            {
                var nextAction = actionQueue.Poll();
                StartState(nextAction);
            }

            // none remaining - go to idle
            // aggressive mode will not allow this to happen
            // idle is literally "nothing to do -> wait for event"
            else
            {
                if (currentActivity != Activity.idle)
                {
                    StartState(AIState.idling);
                }
            }
        }

        /// <summary>
        /// Sets the NPC to a new state
        /// Called when a new state is taken from the queue
        /// i.e. the first frame a state is active
        /// </summary>
        protected void StartState(AIState newState)
        {

            // ** nacpat parametr stavu animace


            currentState = newState;
            // update time remaining
            currentActivityTimer = newState.activityTime;

            npcAnimator.SetInteger("State",(int)currentActivity);


            // call switch
            switch (currentActivity)
            {
                case Activity.idle: // done
                    StartIdle();
                    break;
                case Activity.waiting: // done
                    timeUntilSay = npcFunc.GenerateSpeechDelay(idleChattiness);
                    npcAnimation.OnWaitingStart();
                    break;
                case Activity.aggressive: // done-ish
                    // sometimes say something
                    SaySomethingWithChance(aggressiveSpeech, aggressiveChattiness);
                    //isAggroed = true; // this is done externally
                    //hostileTarget = currentState.goalTransform;
                    break;
                case Activity.sleeping: // done
                    // awakening is done by the time event
                    // teleport to bed //if close enough
                    npcTransform.position = bed.position + 0.3f * Vector3.up;

                    npcNavigation.speed = 0;
                
                    // start animation
                    npcAnimation.OnSleepStart();
                    break;
                case Activity.travelling: // done?
                    timeUntilSay = npcFunc.GenerateSpeechDelay(travelChattiness);
                
                    // set up pathfinding
                    npcNavigation.destination = currentState.currentGoal;
                    npcNavigation.speed = walkingSpeed;
                    // start animation
                    npcAnimation.OnTravelStart();
                    break;
                case Activity.melee: // done
                    // rotate toward goal - this is done by the pathfinding tbh
                    // check if close enough
                    if (npcFunc.PlayerDistanceFromNPC() > meleeMaxDistance)
                    {
                        break;
                    }

                    // possibly todo rng
                    GameState.instance.health -= meleeDamage;
                
                    // start animation
                    npcAnimation.OnMelee();
                
                    break;
                case Activity.shooting: // just reset rtl
                    reloadTimeLeft = 0;
                    npcAnimation.OnHoldGun();

                    // rotate to face enemy
                    
                
                    break;
                case Activity.reloading: // done
                    // nothing else as reload activity automatically expires
                    npcAnimation.OnReload();
                
                    break;
                case Activity.dialogue: // done?
                    Time.timeScale = 0; // TODO move elsewhere?
                    npcDialogue.InitDialogue();
                    break;
                case Activity.death: // done
                    DeathStart();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Called each frame the current state is current
        /// e.g. idle state polling if state has been updated to switch
        /// </summary>
        protected void UpdateState()
        {
            // switch with current action
            switch (currentActivity)
            {
                case Activity.idle: // done
                    // nothing
                    break;
                case Activity.waiting: // done
                    // process say time
                    ProcessSayTime(idleChattiness);
                    break;
                case Activity.aggressive: // done
                    // only here for like 1 frame so actually dont do anything tbh
                    break;
                case Activity.sleeping: // done
                    // check if player is too close -> wake up
                    if (npcFunc.PlayerDistanceFromNPC() < wakeUpDistance)
                    {
                        currentState.activityTime = 0.5f;
                    }
                    break;
                case Activity.travelling: // done

                    if (isAggroed && npcFunc.PlayerDistanceFromNPC() > calmThreshold)
                    {
                        currentState.activityTime = 0.01f;
                    }
                    
                    // process say time
                    if (!isAggroed)
                    {
                        ProcessSayTime(travelChattiness);
                    }
                
                    // recalculate target
                    if (currentState.goalType == GoalType.player)
                    {
                        npcNavigation.destination = currentState.currentGoal;
                    }

                    // if melee and close enough, end state
                    if (isAggroed && !usesWeapon &&
                        npcFunc.PlayerDistanceFromNPC() < 0.9f * meleeMaxDistance)
                    {
                        currentState.activityTime = 0f;
                    }

                    else if (isAggroed && npcFunc.CanSeePlayer())
                    {
                        currentState.activityTime = Mathf.Min(currentState.activityTime, 0.5f);
                    }
                
                    else if (goalDist < maxDistToEndTravel)
                    {
                        currentState.activityTime = 0f;
                    }

                    break;
                case Activity.melee: // done
                    // possibly add movement
                    break;
                case Activity.shooting: // done?

                    // will shoot periodically until time is up
                    reloadTimeLeft -= Time.deltaTime;
                    if (reloadTimeLeft < 0)
                    {
                        // FIRE!!
                        // TODO
                        Fire();

                        reloadTimeLeft = weaponData.intraclip_nominal;
                    }
                
                    // rotate towards player
                    var direction = currentState.currentGoal - npcTransform.position;
                    var lookRotation = Quaternion.LookRotation(direction);

                    if (aimTurnSpeed == 100)
                    {
                        npcTransform.rotation = lookRotation;
                    }
                    else
                    {
                        npcTransform.rotation = Quaternion.Slerp(
                            npcTransform.rotation, lookRotation, Time.deltaTime * aimTurnSpeed);
                    }
                

                    break;
                case Activity.reloading: // done
                    // nothing lol -- rtl is obsolete as the reload state expires automatically
                    break;
                case Activity.dialogue: // done
                    // nothing
                    break;
                case Activity.death: // done?
                    // nothing for now
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        
        }

        /// <summary>
        /// Called the last frame a state is active
        /// i.e. before another new state is taken into the queue
        /// e.g. when timer runs out
        /// e.g. death - destroys object
        /// </summary>
        protected void EndState()
        {
            currentState.callback?.Invoke();

            switch (currentActivity)
            {
                case Activity.idle: // done
                    // do nothing
                    break;
                case Activity.waiting: // done
                    // nothing?
                    break;
                case Activity.aggressive: // done
                    Aggressive();
                    break;
                case Activity.sleeping: // done
                    // teleport infront of bed
                    npcTransform.position = inFrontOfBedPosition;
                    
                    npcNavigation.speed = walkingSpeed;
                
                    // end animation
                    npcAnimation.OnSleepEnd();
                    break;
                case Activity.travelling: // done?
                    npcAnimation.OnTravelEnd();
                
                    npcNavigation.destination = npcTransform.position;
                
                    break;
                case Activity.melee: // done
                    // nothing?
                    break;
                case Activity.shooting: // done
                    npcAnimation.OnHideGun();
                    reloadTimeLeft = 0f; // reset reload time to be sure
                    break;
                case Activity.reloading:
                    break;
                case Activity.dialogue: // done?
                    Time.timeScale = 1f;
                    break;
                case Activity.death: // done
                    DeathEnd();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void Update()
        {
            // check things like death and aggressive and other things
            UpdateNPC();
        
            // update current activity
            UpdateActivityTimer();
        
            // if not -> call updateState()
            UpdateState();
        }

        /// <summary>
        /// Does things like checks max health and shit
        /// </summary>
        protected void UpdateNPC()
        {
            // calc velocity
            if (lastNpcPosition != Vector3.zero)
            {
                var pos = npcTransform.position;
                speed = (pos - lastNpcPosition).magnitude;
                lastNpcPosition = pos;
            }
            
            if (!dead && health <= 0)
            {
                dead = true; // here? or in death start
                EnqueueNewAction(AIState.death);
            }
        
            // check if not aggressive anymore or something, TODO
            if (isAggroed && npcFunc.PlayerDistanceFromNPC() > calmThreshold)
            {
                // isAggroed = false; // instead, check this while walking
            }
        
            else if (!isAggroed && npcFunc.PlayerDistanceFromNPC() < aggravateThreshold)
            {
                isAggroed = true;
                // is this correct? todo?
                EnqueueNewAction(new AIState(Activity.aggressive, 0f, true));
            }
        
        }
    
        /// <summary>
        /// If time has run out, end current state and fetch new one
        /// </summary>
        protected void UpdateActivityTimer()
        {
            // // check if time run out
            // if (currentActivityTimer <= 0)
            // {
            //     Debug.Log("CAT is already zero at beginning of frame!");
            // }
        
            currentActivityTimer -= Time.deltaTime;

            // if time run out -> call EndState and then change state
            if (currentActivityTimer <= 0)
            {
                EndState();
                FetchNewAction();
                return;
            }
        }

        protected void HourlyUpdate(int hour)
        {
            // sleep time
            if (goesToSleep)
            {
                if (hour == sleepTime && currentActivity != Activity.sleeping)
                {
                    // travel to bed
                    AIState goingToBed = new AIState(Activity.travelling, 1000f, true)
                    {
                        goalVector = inFrontOfBedPosition,
                        goalType = GoalType.nonplayer
                    };
                    EnqueueNewAction(goingToBed);
                
                    // set cap on time
                    EnqueueNewAction(new AIState(Activity.sleeping, 
                        WorldTime.HourLength(sleepTime, awakeTime), false));
                }

                if (hour == awakeTime && currentActivity == Activity.sleeping)
                {
                    // just stop sleeping
                    currentState.activityTime = 0.01f;
                    
                    // enqueue travel
                    if (workPosition != Vector3.zero)
                    {
                        AIState goingFromBed = new AIState(Activity.travelling, 1000f, false)
                        {
                            goalVector = workPosition,
                            goalType = GoalType.nonplayer
                        };
                        EnqueueNewAction(goingFromBed);
                    }
                }
            }
        
            // user functions
            OnNewHour(hour);
        }

        #endregion

        #region Helper methods

        protected void SaySomethingWithChance(string[] options, float chance)
        {
            if (options.Length == 0)
            {
                return;
            }
            
            if (chance < Random.value)
            {
                npcFunc.Say(options[Random.Range(0, options.Length)]);
            }
        }

        protected void ProcessSayTime(float currentChattiness)
        {
            // decrement timeUntilSay
            timeUntilSay -= Time.deltaTime;

            if (timeUntilSay < 0f)
            {
                SaySomethingWithChance(idleSpeech, 1);
                timeUntilSay = npcFunc.GenerateSpeechDelay(currentChattiness);
            }
        
        }

        /// <summary>
        /// Also TODO: if has LOS, dont move?
        /// </summary>
        protected void Aggressive()
        {
            // if unagroed, dont do anything
            // TODO problem: this will probs never happen because pursuit of player, FIX!
            if (!isAggroed)
            {
                return;
            }
        
            // enqueue movement, then melee/shooting+reload, then aggressive state
            // dont forget to set time to 0
            AIState moveToPlayer = 
                new AIState(Activity.travelling, 100f, false)
                {
                    goalType = GoalType.player
                };
            // TO/DO travelling parameters like shooting and shit
            // // actually not needed?
        
            EnqueueNewAction(moveToPlayer);

            // todo activityTime
            if (usesWeapon)
            {
                // todo change amount of shooted shots here
                float howMuchOfClipToShoot = 1 - (float)RNG.k(2, 6)/12 * salvoCountVariation;
                float unloadTime = weaponData.intraclip_nominal * (weaponData.clip_nominal * howMuchOfClipToShoot);
                AIState shootPlayer = new AIState(Activity.shooting, unloadTime, false)
                {
                    goalType = GoalType.player
                };
                EnqueueNewAction(shootPlayer);

                // reload for given time
                AIState reload = new AIState(Activity.reloading, reloadCoef * weaponData.interclip, false);
                EnqueueNewAction(reload);
            }
            else
            {
                AIState meleePlayer = new AIState(Activity.melee, 3f, false)
                {
                    goalType = GoalType.player
                };
                EnqueueNewAction(meleePlayer);
            }

            AIState aggroed = AIState.aggroed;
            EnqueueNewAction(aggroed);
        }

        protected void DeathStart()
        {
            dead = true;
            WorldTime.OnNewHour -= HourlyUpdate;
        
            npcAnimation.OnDeathStart();
        }
    
        protected void DeathEnd()
        {
            if (dropWeaponChance < Random.value)
            {
                ItemWeaponTrigger.Instantiate(npcTransform.position, weaponData, AmmoType.none, 0);
            }

            AmmoTrigger.Instantiate(npcTransform.position, dropAmmoType, RNG.AmmoDrop(dropAmmoQuantity),
                AmmoType.none, 0);
        
            //npcAnimation.OnDeathEnd();
        
            Destroy(npcTransform.gameObject);
        }

        protected void Fire()
        {
            // todo fire from gun position?

            // Vector3 direction = GameObject.FindGameObjectWithTag("Player").transform.position;
            // GameObject tempBullet = Instantiate(
            //     bullet, shootPoint.transform.position, Quaternion.identity, transform);
            // tempBullet.GetComponent<MoveBullet>().hitPoint = direction;
            BulletSpawner.SpawnNewBullet(
                from: shootPoint.transform.position,
                // to: currentState.currentGoal + Vector3.up,
                to: shootPoint.transform.position + npcTransform.forward,
                dispersion: weaponData.dispersion_real(weaponData.maximumCaliber),
                damage: AmmoData.GetDamageFromCaliber(weaponData.maximumCaliber));
        }

        #endregion

        #region Specific methods

        /// <summary>
        /// Do something from the very beginning
        /// e.g. zombie goes to sleep
        /// Usually nothing, idle will go directly to first activity
        /// </summary>
        protected virtual void OnStart()
        {
            EnqueueNewAction(initState);
            FetchNewAction();
        }

        /// <summary>
        /// Called when idle mode is reached.
        /// Idle mode is a mode where the NPC does nothing and has nothing to do
        /// Default: go to waiting mode
        /// example: guy going back and forth
        /// </summary>
        protected virtual void StartIdle()
        {
            EnqueueNewAction(new AIState(Activity.waiting, float.PositiveInfinity, false));
        }

        /// <summary>
        /// Called by event from WorldState
        /// Typically enqueues actions (19h -> go to sleep)
        /// </summary>
        protected virtual void OnNewHour(int hour)
        {
            // default: nothing
        }

        #endregion
        
    }
}

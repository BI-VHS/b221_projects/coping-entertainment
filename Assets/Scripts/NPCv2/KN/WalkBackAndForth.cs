using System.Collections;
using System.Collections.Generic;
using NPCv2;
using UnityEngine;

public class WalkBackAndForth : NPCState
{

    public Transform from;
    public Transform to;
    
    protected override void StartIdle()
    {
        EnqueueNewAction(new AIState(Activity.travelling, float.PositiveInfinity, true)
        {
            goalType = GoalType.nonplayer,
            goalVector = from.position
        });
        
        EnqueueNewAction(new AIState(Activity.travelling, float.PositiveInfinity, false)
        {
            goalType = GoalType.nonplayer,
            goalVector = to.position
        });
    }
}

using System;
using UnityEngine;

namespace NPCv2
{
    public class BulletSpawner : MonoBehaviour
    {
        [SerializeField] 
        private GameObject bullet;
        
        private static BulletSpawner BS;

        private void Awake()
        {
            BS = this;
        }

        /// <summary>
        /// Spawns bullet at position "from", in the direction of "to"
        /// Damage is also specified
        /// </summary>
        public static void SpawnNewBullet(Vector3 from, Vector3 to, int damage, float dispersion)
        {
            GameObject tempBullet = Instantiate(
                BS.bullet, from, Quaternion.identity);
            
            tempBullet.GetComponent<MoveBullet>().hitPoint = to;
            tempBullet.GetComponent<MoveBullet>().damage = damage;
            tempBullet.GetComponent<MoveBullet>().dispersion = dispersion;
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// NPC dialogue wrapper 
/// </summary>
public class NPCDialogue : MonoBehaviour
{
    private NPCFunctionality npcFunc;
    
    private bool talkedYet;

    [SerializeField] public string NPCName;

    [SerializeField] [Tooltip("NPC vybere z těchto možností první, která splňuje podmínky v hlavičce")]
    private DialogueChooser[] dialogues;
    
    // Start is called before the first frame update
    void Start()
    {
        npcFunc = GetComponent<NPCFunctionality>();
        talkedYet = false;
    }

    public void InitDialogue()
    {
        Debug.Log("Init dialogue");
        
        // time is stopped so do this preventively
        Tooltip.Hide();

        foreach (var dialogue in dialogues)
        {
            if (ConditionEvaluator(dialogue.conditions))
            {
                ProcessDialogueNode(dialogue.dialogueTree);
                break;
            }
            
        }

        talkedYet = true;
    }

    private void ProcessDialogueNode(DialogueNode dialogueNode)
    {
        // 1) update world
        UpdateEvaluator(dialogueNode.updates);
        
        // 2.0) if no dialogue options, just say text and end.
        int dialogueOptionCount = dialogueNode.playerOptionDialogueTrees.Length;
        if (dialogueOptionCount == 0)
        {
            npcFunc.Say(dialogueNode.NPCDialogue);
            return;
        }
        
        // 2) run dialogue and advance in tree
        string dialogueText = dialogueNode.NPCDialogue;
        string cText = dialogueNode.playerOptionTexts[0];
        string zText = dialogueOptionCount == 2 ? dialogueNode.playerOptionTexts[1] : "";

        InteractionManager.instance.ShowDialogue(NPCName, dialogueText, zText, cText,
            result =>
            {
                ProcessDialogueNode(result == InteractionResult.positive
                    ? dialogueNode.playerOptionDialogueTrees[0]
                    : dialogueNode.playerOptionDialogueTrees[1]);
            });
    }

    private void UpdateEvaluator(string[] results)
    {
        foreach (var result in results)
        {
            var parts = result.Split(" ", 2);

            string command = parts[0];
            string varname = parts[1];

            switch (command)
            {
                case "queststart":
                    QuestManager.instance.StartQuest(varname);
                    break;
                case "questcomplete":
                    QuestManager.instance.EndQuest(varname, true);
                    break;
                case "questfail":
                    QuestManager.instance.EndQuest(varname, false);
                    break;
                case "questcontinue":
                    QuestManager.instance.SetQuestWaypoint(varname);
                    break;
                case "increment":
                    WorldState.UpdateValue(varname);
                    break;
                default:
                    throw new ConstraintException();
            }

        }
    }

    private bool ConditionEvaluator(string[] conditions)
    {
        foreach (var cond in conditions)
        { 
            var parts = cond.Split(" ", 3);
            if (parts.Length != 3)
            {
                Debug.LogAssertion("Malformed expression to evaluate! Did you include spaces?");
                return false;
            }

            if (parts[0] == "quest")
            {
                QuestStatus questStatus = parts[1] switch
                {
                    "future" => QuestStatus.future,
                    "current" => QuestStatus.current,
                    "completed" => QuestStatus.completed,
                    "failed" => QuestStatus.failed,
                    _ => throw new InvalidEnumArgumentException()
                };

                QuestData qd = QuestManager.instance.GetQuestData(parts[2]);

                if (qd.questStatus != questStatus)
                {
                    return false;
                }
            }
            else if (parts[0] == "queststate") // queststate 3 Weapons Hot
            {
                string questName = parts[2];
                int questWaypoint = Convert.ToInt32(parts[1]);
                
                QuestData qd = QuestManager.instance.GetQuestData(questName);
                if (qd.currentWaypoint != questWaypoint)
                {
                    return false;
                }
            }
            else // is normal comparison
            {
                int variableValue = parts[0] switch
                {
                    "interacted" => Convert.ToInt32(talkedYet),
                    _ => WorldState.GetValue(parts[0])
                };
                string comparator = parts[1];
                string other = parts[2];

                if (comparator == "==" && other == "true")
                {
                    comparator = ">";
                    other = "0";
                }

                if (comparator == "==" && other == "false")
                {
                    comparator = "==";
                    other = "0";
                }

                int newOther = Convert.ToInt32(other);
                bool res = comparator switch
                {
                    "==" => variableValue == newOther,
                    ">=" => variableValue >= newOther,
                    "<=" => variableValue <= newOther,
                    "<" => variableValue < newOther,
                    ">" => variableValue > newOther,
                    _ => throw new ArgumentOutOfRangeException()
                };

                if (!res)
                {
                    return false;
                }
            }
        }
        
        return true;
    }
    
}

// public enum DialogueNodeType
// {
//     none, 
//     result, clickthough, choice
// }

// public enum DialogueConditionType
// {
//     lesser, greater,
//     equal,
//     lesserOrEqual, greaterOrEqual
// }
//
// [System.Serializable]
// public class DialogueCondition
// {
//     [SerializeField] private string variable;
//     [SerializeField] private DialogueConditionType conditionType;
//     [SerializeField] private int value;
// }


[System.Serializable]
public class DialogueChooser
{
    /// <summary>
    /// e.g. talked==false, asdas=1, sadas>3
    /// logical AND between all
    /// chooser goes top-to-bottom and finds first applicable. None = aborts
    /// </summary>
    [SerializeField] [Tooltip("Textově zadaná sada podmínek, které musí všechny platit, " +
                              "aby se spustil daný dialogový strom. Nemusí být žádné.")]
    public string[] conditions;

    [SerializeField] [Tooltip("Dialogový strom, který se provede, pokud byly podmínky platné.")]
    public DialogueNode dialogueTree;
}

// [System.Serializable]
// public struct DialogueOptions
// {
//     [SerializeField] [Tooltip("Hráč odpovídá: ...")]
//     private string dialogueOption;
//
//     [SerializeField] [Tooltip("Co se stane dál.")]
//     private DialogueNode nextDialogue;
// }

[System.Serializable]
public class DialogueNode
{

    [FormerlySerializedAs("updatedVariables")] [SerializeField] [Tooltip("Jména upravovaných proměnných. K proměnným se přičte 1. Výchozí hodnota je 0.")]
    public string[] updates;

    [SerializeField] [Tooltip("NPC říká: ...")]
    public string NPCDialogue;

    [SerializeField] [Tooltip("Co může říkat hráč. 0 možností=konec dialogu, 1=proklikávací, 2=má na výběr. " +
                              "První možnost je kladnější (pravá - c), druhá je zápornější (levá - z).")]
    public string[] playerOptionTexts;

    [SerializeField] [Tooltip("Co se stane, když hráč něco řekne. 0 možností=konec dialogu, 1=proklikávací, 2=má na výběr")]
    public DialogueNode[] playerOptionDialogueTrees;



}



using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public enum NPCStateEnum
{
    none,
    idle, 
    goingToPosition,
    firing, reloading

}

public class NPCGeneric : MonoBehaviour
{
    public TextMeshProUGUI sayText;
    private float sayTimeRemaining;

    // public Transform canvasTransform;
    // public Transform parentTransform;

    /// <summary>
    /// More chatty = more idle text
    /// </summary>
    public float chattiness = 1f;

    public float fireTime = 1f;
    public float reloadTime = 5f;

    public string[] idleSpeech;

    public NPCStateEnum NpcStateEnum { get; protected set; }
    private float stateTimeRemaining;
    private NPCStateEnum nextStateEnum;

    /// <summary>
    /// Goes to first one, then next...
    /// </summary>
    private List<Vector3> goalPositionList;

    /// <summary>
    /// Hello, my name is:
    /// </summary>
    public void Say(string sentence)
    {
        sayText.text = sentence;

        int ln = sentence.Length;
        sayTimeRemaining = ln == 0 ? 0 : (float)ln / 10 + 1;
    }

    /// <summary>
    /// New place for NPC to go
    /// </summary>
    public void AddNewGoalPosition(Vector3 newGoalPosition)
    {
        goalPositionList.Add(newGoalPosition);
        SetState(NPCStateEnum.goingToPosition);
    }

    // ReSharper disable Unity.PerformanceAnalysis
    protected void SetState(NPCStateEnum stateEnum)
    {
        NpcStateEnum = stateEnum;
        switch (NpcStateEnum)
        {
            case NPCStateEnum.idle:
                stateTimeRemaining = float.PositiveInfinity;
                StartingToIdle();
                break;
            case NPCStateEnum.goingToPosition:
                stateTimeRemaining = float.PositiveInfinity; 
                GoingToPosition();
                break;
            case NPCStateEnum.firing:
                stateTimeRemaining = fireTime;
                Firing();
                break;
            case NPCStateEnum.reloading:
                stateTimeRemaining = reloadTime;
                Reloading();
                break;
            default:
                Debug.LogAssertion("NPC in defunct state!");
                break;
        }
    }

    /// <summary>
    /// Called each frame the NPC is idle
    /// </summary>
    protected virtual void UpdateIdle()
    {
        
    }

    private void UpdateGoingToPosition()
    {
        if (goalPositionList.Count == 0)
        {
            SetState(NPCStateEnum.idle);
        }

        Vector3 dist = transform.position - goalPositionList[0];
        if (dist.magnitude < 1f)
        {
            goalPositionList.RemoveAt(0);
        }
    }

    /// <summary>
    /// Called on the first frame the NPC becomes idle
    /// </summary>
    protected virtual void StartingToIdle()
    {
        // TODO idle animation
        Say("Idling...");
    }

    /// <summary>
    /// Called on the first frame the NPC starts to go somewhere
    /// </summary>
    protected virtual void GoingToPosition()
    {
        // TODO walking animation
        Say("Going to a new position...");
    }

    /// <summary>
    /// Called on the first frame the NPC starts to shoot in cycle
    /// What to do when Firing is commenced
    /// </summary>
    protected virtual void Firing()
    {
        // TODO firing animation
        Say(Random.value < 0.5 ? "Pew pew!" : "Another miss...");

        // set next state
        nextStateEnum = Random.value < 0.5 ? NPCStateEnum.firing : NPCStateEnum.reloading;
    }

    /// <summary>
    /// Called on the first frame the NPC starts to reload
    /// What to do when reloading is commenced
    /// </summary>
    protected virtual void Reloading()
    {
        // TODO reloading animation
        Say("Reloading...");

        nextStateEnum = Random.value < 0.3 ? NPCStateEnum.idle : NPCStateEnum.firing;
    }

    protected bool DeltaProb(float chance)
    {
        return Random.value < chance * Time.deltaTime;
    }

    private void SayIdleSomething()
    {
        if (idleSpeech.Length == 0 || sayTimeRemaining > 0)
        {
            return;
        }
        
        if (DeltaProb(0.1f * chattiness))
        {
            Say(idleSpeech[Random.Range(0, idleSpeech.Length)]);
        }
    }
    

    private void Update()
    {
        if (NpcStateEnum == NPCStateEnum.idle)
        {
            SayIdleSomething();
            UpdateIdle();
        }

        else if (NpcStateEnum == NPCStateEnum.goingToPosition)
        {
            UpdateGoingToPosition();
        }

        UpdateState();
        
        UpdateSpeech();
    }

    private void UpdateState()
    {
        if (stateTimeRemaining > 0)
        {
            stateTimeRemaining -= Time.deltaTime;
            if (stateTimeRemaining <= 0)
            {
                NPCStateEnum newStateEnum = nextStateEnum == NPCStateEnum.none ? NPCStateEnum.idle : nextStateEnum;
                SetState(newStateEnum);
            }
        }
    }

    private void UpdateSpeech()
    {
        if (sayTimeRemaining > 0)
        {
            sayTimeRemaining -= Time.deltaTime;
            if (sayTimeRemaining <= 0)
            {
                Say("");
            }
        }
    }

    private void Start()
    {
        sayTimeRemaining = 0;
        nextStateEnum = NPCStateEnum.none;
        stateTimeRemaining = float.PositiveInfinity;
        
        SetState(NPCStateEnum.idle);
    }
}




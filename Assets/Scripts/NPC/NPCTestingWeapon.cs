using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Fire and reload is where it's at
/// </summary>
public class NPCTestingWeapon : NPCGeneric
{
    protected override void UpdateIdle()
    {
        if (DeltaProb(0.2f))
        {
            SetState(NPCStateEnum.firing);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldTime : MonoBehaviour
{
    [Tooltip("Kolik je hodin, když začne hra. Doporučeno ne 0")] [Range(0, 23)] [SerializeField]
    private int defaultHour = 8;
    
    private float _time;

    [Tooltip("Kolik vteřin trvá hodina")] [Range(1, 1000)] [SerializeField]
    private int secondsInHour = 100;
        
    private int secondsInDay => secondsInHour * 24;

    private int currentHour;
    private int currentDay;

    public delegate void NewHour(int hour);
    
    // called exactly once per in-game hour
    public static event NewHour OnNewHour;

    public delegate void NewDay(int day);
    public static event NewDay OnNewDay;

    private static WorldTime instance;
    
    // Start is called before the first frame update
    private void Start()
    {
        _time = defaultHour * secondsInHour;
        currentDay = 0;
        currentHour = defaultHour;

        instance = this;

        //OnNewHour += HourLogger;
    }

    // Update is called once per frame
    private void Update()
    {
        _time += Time.deltaTime;
        int newHour = (int)_time / secondsInHour;
        int newDay = (int) _time / secondsInDay;
        if (newHour > currentHour)
        {
            OnNewHour?.Invoke(newHour % 24);
            // Debug.Log($"new hour event: {newHour % 24}!");

            if (newHour % 24 == 0)
            {
                OnNewDay?.Invoke(newDay);
            }

            currentHour = newHour;
            currentDay = newDay;
        }
    }

    /*private void HourLogger(int hour)
    {
        Debug.Log($"new hour event: {hour}!");
    }*/

    public static int HourLength(int from, int to)
    {
        if (to < from)
        {
            to += 24;
        }

        return (to - from) * instance.secondsInHour;
    }

    public static GameTime GetTime()
    {
        return new GameTime
        {
            day = (int) instance._time / instance.secondsInDay,
            hour = (instance._time / instance.secondsInHour) % 24
        };
    }
}

public struct GameTime
{
    public int day;
    public float hour;
}
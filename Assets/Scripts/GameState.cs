using System;
using System.Collections;
using System.Collections.Generic;
using NPCv2;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public enum AmmoType
{
 mm556 = 0, mm762 = 1, mm858 = 2,
 none = -1
}

public enum ReloadType
{
    none, intra, inter
}

/// <summary>
/// Manages info about the current game state.
/// e.g. hp, stats
/// todo extend to other stats like position, current level, etc.
/// currently covers: health, maxhealth, karma, ammo
/// </summary>
public class GameState : MonoBehaviour
{
    [SerializeField] private int _health;
    [SerializeField] private int _maxhealth;
    [SerializeField] private float _karma;
    [SerializeField] private WeaponData _weapon;
    [SerializeField] private AmmoData[] _ammo;
    [SerializeField] private float _reloadTimeLeft;
    [SerializeField] private ReloadType _reloadType;

    public QuestManager questManager;

    public GameObject bullet;
    public GameObject shootPoint;

    /// <summary>
    /// current player health
    /// </summary>
    public int health
    {
        get => _health;
        set
        {
            _health = Mathf.Clamp(value, 0, maxHealth);
            PlayerStateView.instance.SetHealth(_health, maxHealth);

            if (health <= 0)
            {
                Death();
            }
        }
    }

    /// <summary>
    /// maximum player health can also increase
    /// scales current health with obtained health
    /// </summary>
    public int maxHealth
    {
        get => _maxhealth;
        set
        {
            float ratio = (float)value / _maxhealth;
            _maxhealth = value;
            _health = (int) Math.Round(_health * ratio);
            PlayerStateView.instance.SetHealth(health, _maxhealth);
        }
    }

    /// <summary>
    /// 0 - neutral
    /// positive - positive
    /// negative - negative
    /// </summary>
    public float karma
    {
        get => _karma;
        set => _karma = value;
    }

    /// <summary>
    /// SO's of all three ammo types
    /// </summary>
    public AmmoData[] ammoData
    {
        get => _ammo;
        private set => _ammo = value;
    }

    
    /// <summary>
    /// The player's current weapon
    /// </summary>
    public WeaponData weapon
    {
        get => _weapon;
        set
        {
            _weapon = value;
            PlayerStateView.instance.SetWeapon(value);
            RecalculateSelected();
            Reload();
        }
    }

    /// <summary>
    /// how much time left on relaod
    /// </summary>
    public float reloadTimeLeft { 
        get => _reloadTimeLeft;
        private set => _reloadTimeLeft = value;
    }

    /// <summary>
    /// True iff weapon currently reloaded
    /// </summary>
    public bool canFire => reloadTimeLeft == 0;

    private float totalLoadTime;

    /// <summary>
    /// Currently remaining loaded ammo count
    /// </summary>
    public int clipRemaining;

    /// <summary>
    /// Currently selected ammo type. 
    /// </summary>
    public AmmoType selectedAmmoType;

    public AmmoData selectedAmmo
    {
        get { return ammoData[(int) selectedAmmoType]; }
    }

    /// <summary>
    /// Reload type
    /// </summary>
    public ReloadType reloadType
    {
        get;
        private set;
    }

    /// <summary>
    /// amount of ammo types
    /// </summary>
    public const int ammoTypesCount = 3;
    
    public static GameState instance;

    /// <summary>
    /// Pass argument to increase ammo count
    /// </summary>
    public void CollectPickup(AmmoData ammoPickupData)
    {
        ChangeInAmmoCount(ammoPickupData.caliber, ammoPickupData.amount);
        RecalculateSelected();
    }

    /// <summary>
    /// Changes ammo count by given amount. Does not spot from having negative ammo!
    /// </summary>
    public void ChangeInAmmoCount(AmmoType ammoType, int amount)
    {
        if (ammoData[(int) ammoType].amount + amount < 0)
        {
            Debug.LogAssertion("Setting ammo count to negative!");
        }
        
        ammoData[(int) ammoType].amount += amount;
        RecalculateSelected();
    }

    /// <summary>
    /// Applies quest reward
    /// </summary>
    public void GetQuestReward(int dmaxhealth, float dkarma, int[] dammos)
    {
        maxHealth += dmaxhealth;
        karma += dkarma;

        for (int i = 0; i < ammoData.Length; i++)
        {
            ammoData[i].amount += dammos[i];
        }
        
        RecalculateSelected();
    }

    /// <summary>
    /// Attempts selecting an ammo type
    /// </summary>
    public void SelectAmmo(AmmoType newSelectedAmmoType)
    {
        if (ammoData[(int) newSelectedAmmoType].amount == 0 || !weapon.isCompatible(newSelectedAmmoType))
        {
            return;
        }        
        
        if (selectedAmmoType != newSelectedAmmoType)
        {
            InitReload(weapon.interclip, ReloadType.inter);
        }

        selectedAmmoType = newSelectedAmmoType;

        RecalculateSelected();
    }

    public bool IsAmmoTypeAvailable(AmmoType ammoType)
    {
        int at = (int) ammoType;
        return ammoData[at].amount > 0 && ammoData[at].caliber <= weapon.maximumCaliber;
    }

    private UseStatus AmmoUseStatus(AmmoType ammoType)
    {
        return ammoType == selectedAmmoType ? UseStatus.active :
            IsAmmoTypeAvailable(ammoType) ? UseStatus.available : UseStatus.unavailable;
    }

    private void RecalculateSelected()
    {
        bool mini = IsAmmoTypeAvailable(AmmoType.mm556);
        bool midi = IsAmmoTypeAvailable(AmmoType.mm762);
        bool maxi = IsAmmoTypeAvailable(AmmoType.mm858);

        int typesAvailable = Convert.ToInt32(mini) + Convert.ToInt32(midi) + Convert.ToInt32(maxi);
        
        // this is the best line ever
        AmmoType largestAvailable =
            maxi ? AmmoType.mm858 : midi ? AmmoType.mm762 : mini ? AmmoType.mm556 : AmmoType.none;
        
        // if none -- set to none
        if (typesAvailable == 0)
        {
            selectedAmmoType = AmmoType.none;
        }
        
        // if only one ammo type -- select that one
        else if (typesAvailable == 1)
        {
            selectedAmmoType = largestAvailable;
        }
        
        // multiple choices of ammo available
        else
        {
            // if current none, select largest available
            if (selectedAmmoType == AmmoType.none)
            {
                selectedAmmoType = largestAvailable;
            }

            // if current empty, select largest available
            if (selectedAmmo.amount == 0)
            {
                selectedAmmoType = largestAvailable;
            }

            // if current not available, select largest available
            if (selectedAmmoType > largestAvailable)
            {
                selectedAmmoType = largestAvailable;
            }
            
            // remaining cases:
            // - selected is largest available
            // - selected is other avaiable type
            // -> no action needed
            
        }
        
        PlayerStateView.instance.SetActiveAmmo(
            new []{AmmoUseStatus(AmmoType.mm556), AmmoUseStatus(AmmoType.mm762), AmmoUseStatus(AmmoType.mm858)}
            );
    }

    /// <summary>
    /// Triggers new reload of current ammo
    /// </summary>
    public void Reload()
    {
        clipRemaining = 0;
        InitReload(weapon.interclip, ReloadType.inter);
    }

    private void InitReload(float time, ReloadType newReloadType)
    {
        if (selectedAmmoType == AmmoType.none)
        {
            return;
        }
        
        reloadTimeLeft = time;
        totalLoadTime = time;

        reloadType = newReloadType;
        
        PlayerStateView.instance.SetWeaponClipRemaining(0, weapon.clip_real(selectedAmmoType));
    }

    private void FinishReload()
    {
        if (selectedAmmoType == AmmoType.none)
        {
            clipRemaining = 0;
            reloadType = ReloadType.none;
            return;
        }

        if (reloadType == ReloadType.inter)
        {
            clipRemaining = Math.Min(selectedAmmo.amount, weapon.clip_real(selectedAmmoType));
        }

        PlayerStateView.instance.SetWeaponClipRemaining(clipRemaining, weapon.clip_real(selectedAmmoType));
    }

    /// <summary>
    /// Attempts shooting the gun
    /// </summary>
    public void Fire()
    {
        

        if (clipRemaining == 0 || reloadTimeLeft > 0 || selectedAmmo.amount == 0)
        {
            return;
        }

       
        if (clipRemaining > 0 && reloadTimeLeft == 0){
            
            Shoot(); 
        }
        clipRemaining--;
        selectedAmmo.amount--;

        if (clipRemaining == 0)
        {
            // long reload
            InitReload(weapon.interclip, ReloadType.inter);
        }
        else
        {
            //short reload
            InitReload(weapon.intraclip_real(selectedAmmoType), ReloadType.intra);
        }
        
        PlayerStateView.instance.SetAmmoCount(selectedAmmoType, selectedAmmo.amount);
        PlayerStateView.instance.SetWeaponClipRemaining(clipRemaining, weapon.clip_real(selectedAmmoType));
        
        RecalculateSelected();
    }

    public void UpdateUI()
    {
        PlayerStateView.instance.SetHealth(health, maxHealth);
        PlayerStateView.instance.SetWeapon(weapon);
        PlayerStateView.instance.SetAmmoDataInView(ammoData);
        PlayerStateView.instance.SetWeaponLoadStatus(1);
        
        // TODO
        PlayerStateView.instance.SetActiveAmmo(new [] {UseStatus.active, UseStatus.available, UseStatus.available});
        PlayerStateView.instance.SetWeaponClipRemaining(clipRemaining, weapon.clip_real(selectedAmmoType));
        
        Reload();
    }
    
    // Start is called before the first frame update
    private void Start()
    {
        //oink oink
        for (int i = 0; i < ammoData.Length; i++)
        {
            ammoData[i] = Instantiate(ammoData[i]);
        }
        
        
        instance = this;

        // InteractionManager.instance.BuyWeaponWrapped(weapon, AmmoType.mm556, 20);
    }

    private void Update()
    {
        UpdateLoadWeapon();
    }

    private void UpdateLoadWeapon()
    {
        if (reloadTimeLeft > 0)
        {
            reloadTimeLeft -= Time.deltaTime;
            if (reloadTimeLeft <= 0)
            {
                FinishReload();
                reloadTimeLeft = 0;
            }

            if (reloadType == ReloadType.inter)
            {
                PlayerStateView.instance.SetWeaponLoadStatus(reloadTimeLeft / totalLoadTime);
            }
        }
    }

    private void Shoot()
    {
        Vector3 shootAtPos = Controls.GetWorldMousePos();
        shootAtPos.y = Mathf.Lerp(shootAtPos.y, 1, 0.5f);
        
        // GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, Quaternion.identity, transform);
        // MoveBullet bulletData = tempBullet.GetComponent<MoveBullet>();
        // bulletData.hitPoint = shootAtPos;
        // bulletData.damage = selectedAmmo.damage;
        // bulletData.dispersion = weapon.dispersion_real(selectedAmmoType);

        BulletSpawner.SpawnNewBullet(
            from: shootPoint.transform.position,
            // to: currentState.currentGoal + Vector3.up,
            to: shootAtPos,
            dispersion: weapon.dispersion_real(selectedAmmoType),
            damage: selectedAmmo.damage);
    }

    private void Death()
    {
        Debug.Log("Player died. Lol.");
        
        UIManager.instance.LoadScene(0);
    }
}


public static class RNG
{
    /// <summary>
    /// Computes akb, for example 2k6
    /// </summary>
    public static int k(int a, int b)
    {
        int sum = 0;
        for (int i = 0; i < a; i++)
        {
            sum += Random.Range(0, b+1);
        }

        return sum;
    }

    /// <summary>
    /// Suggested use for ammo drops.
    /// </summary>
    public static int AmmoDrop(int count)
    {
        // https://anydice.com/program/2c6b8
        return Mathf.Max(0, k(2 * count, 6) - 6 * count);
    }
    
}


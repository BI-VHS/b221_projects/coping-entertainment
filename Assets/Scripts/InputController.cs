using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public GameState gamestate;
    
    public static InputController instance;
    
    // Start is called before the first frame update
    private void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    private void Update()
    {
        if (Controls.GetButtonDownGame("reload")) 
            gamestate.Reload();

        if (Controls.GetButtonDownGame("load1")) gamestate.SelectAmmo(AmmoType.mm556); 
        if (Controls.GetButtonDownGame("load2")) gamestate.SelectAmmo(AmmoType.mm762); 
        if (Controls.GetButtonDownGame("load3")) gamestate.SelectAmmo(AmmoType.mm858); 

        if (Controls.GetButtonGame("Fire1")){
            
            // print(MathF.Abs(PlayerController.instance.getRotation().x));
            // print(MathF.Abs(PlayerController.instance.getRotation().y));
            gamestate.Fire();
            
        }
           
    }
}

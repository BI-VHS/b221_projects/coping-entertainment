using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBulletNPC : MonoBehaviour
{
    public Vector3 hitPoint;

    private Vector3 direction;

    public int speed;
    // Start is called before the first frame update
    void Start()
    {
        direction.x = hitPoint.x;
        direction.y = this.transform.position.y;
        direction.z = hitPoint.z;

        var childPlayer = GameObject.FindGameObjectWithTag("Player");
        var mousePos = Controls.GetWorldMousePos();

        var lookPos = mousePos - childPlayer.transform.position;
        var rotation = Quaternion.LookRotation(lookPos);

        this.transform.rotation = rotation;
        this.GetComponent<Rigidbody>().AddForce((hitPoint - this.transform.position).normalized * speed);


    }


    // Update is called once per frame
    void Update()
    {


    }

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Npc")
        {
            print("Trafil si npc ty chuj");
        }
        Destroy(this.gameObject);
    }

}

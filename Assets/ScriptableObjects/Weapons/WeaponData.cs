
using System;
using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// Data for weapons
/// </summary>
[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 1)]
public class WeaponData : ScriptableObject
{
    /// <summary>
    /// A short name
    /// </summary>
    [FormerlySerializedAs("name")] 
    public string weaponName;
    
    /// <summary>
    /// Long description for tooltip
    /// </summary>
    public string description;

    /// <summary>
    /// Weapon cannot use bullets of higher caliber
    /// </summary>
    public AmmoType maximumCaliber;
    
    /// <summary>
    /// ROF-time
    /// </summary>
    public float intraclip_nominal;
    
    /// <summary>
    /// Reload time
    /// </summary>
    public float interclip;
    
    /// <summary>
    /// Clip size
    /// </summary>
    public int clip_nominal;

    /// <summary>
    /// 0 - fully accurate for the preferred caliber
    /// x - x% inacurracy
    /// </summary>
    public float dispersion_nominal;

    /// <summary>
    /// UI image
    /// </summary>
    public Sprite sprite;

    /// <summary>
    /// Guns can only use ammo of lower caliber
    /// </summary>
    /// <returns></returns>
    public bool isCompatible(AmmoType ammoSize)
    {
        return ammoSize <= maximumCaliber;
    }
    
    /// <summary>
    /// Calculates "buff" when using subpar ammo
    /// e.g. .45 gun + .38 ammo = faster ROF
    /// </summary>
    public float intraclip_real(AmmoType ammoSize)
    {
        return (maximumCaliber - ammoSize) switch
        {
            0 => intraclip_nominal,
            1 => intraclip_nominal / 1.5f,
            2 => intraclip_nominal / 5f,
            _ => throw new ArgumentException()
        };
    }

    /// <summary>
    /// Calculates nerf when using subpar ammo
    /// e.g. .45 gun + .38 ammo = faster ROF
    /// </summary>
    public float dispersion_real(AmmoType ammoSize)
    {
        return (maximumCaliber - ammoSize) switch
        {
            0 => dispersion_nominal,
            1 => dispersion_nominal + 3,
            2 => dispersion_nominal + 12,
            _ => throw new ArgumentException()
        };
    }
    
    /// <summary>
    /// Calculates "buff" when using subpar ammo
    /// e.g. .45 gun + .38 ammo = faster ROF
    /// </summary>
    public int clip_real(AmmoType ammoSize)
    {
        return (maximumCaliber - ammoSize) switch
        {
            0 => clip_nominal,
            1 => clip_nominal * 2,
            2 => clip_nominal * 3,
            _ => throw new ArgumentException()
        };
    }

}
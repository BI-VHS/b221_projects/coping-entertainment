using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Data for ammo
/// </summary>
[CreateAssetMenu(fileName = "AmmoData", menuName = "ScriptableObjects/AmmoData", order = 2)]
public class AmmoData : ScriptableObject
{
    public static int[] damageStats = new[]
    {
        10, 15, 60
    };
    
    public string nameShort;
    public string nameLong;
    
    /// <summary>
    /// caliber
    /// </summary>
    public AmmoType caliber;

    /// <summary>
    /// nominal damage upon hit
    /// </summary>
    public int damage => GetDamageFromCaliber(caliber);
    

    public static int GetDamageFromCaliber(AmmoType cal)
    {
        return cal == AmmoType.none ? 0 : damageStats[(int) cal];
    }

    /// <summary>
    /// For player or for pickup - how much ammo
    /// </summary>
    public int amount;

    /// <summary>
    /// UI image
    /// </summary>
    public Sprite spriteUI;
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/QuestData", order = 3)]
public class QuestData : ScriptableObject
{
    /// <summary>
    /// Name of the quest. 1-3 words
    /// Must be unique!
    /// </summary>
    public string questName;
    
    /// <summary>
    /// Full description - one paragraph. Shown in tooltip.
    /// </summary>
    [TextArea(5,20)]
    public string description;

    /// <summary>
    /// Current waypoint on the map.
    /// </summary>
    public int currentWaypoint = 0;

    /// <summary>
    /// 0 - no waypoints
    /// 1+ - quest will always have at least one waypoint
    /// </summary>
    public int totalWaypoints => waypoints_scenes.Length;

    /// <summary>
    /// Total waypoints 2 -> 0, 1
    /// </summary>
    public bool showWaypoint => totalWaypoints > 0 && currentWaypoint < totalWaypoints;

    /// <summary>
    /// The current waypoint
    /// </summary>
    public Tuple<int, Vector3> waypoint => new(waypoints_scenes[currentWaypoint], waypoints_coords[currentWaypoint]);
    
    /// <summary>
    /// List of coordinates the player has to go to in form:
    /// scene number, x, y, z.
    /// Set to -inf to make invisible
    /// TODO refactor
    /// </summary>
    public int[] waypoints_scenes;
    public Vector3[] waypoints_coords;

    /// <summary>
    /// The current status of the quest
    /// </summary>
    public QuestStatus questStatus;

    /// <summary>
    /// How much karma the player gets for finishing the quest
    /// Only apply to soldier quests
    /// </summary>
    public float karmaGained;
    
    /// <summary>
    /// Ammo gained as reward for smuggling quests
    /// </summary>
    public int[] ammoGained;

    /// <summary>
    /// Use none for each normal quest.
    /// </summary>
    public SpecialQuestResult specialQuestResult;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TESTNPC : MonoBehaviour

{
    public GameObject bullet;
    public GameObject shootPoint;

    private float nextActionTime = 0.0f;
    public float period = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Vector3 npcPos = gameObject.transform.position;
        
        float minDist = 5;
        float dist = Vector3.Distance(playerPos, npcPos);
        if (dist < minDist)
        {
            Shoot();
        }
    }

    public void Shoot()
    {

        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            Vector3 direction = GameObject.FindGameObjectWithTag("Player").transform.position;
            GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, Quaternion.identity, transform);
            tempBullet.GetComponent<MoveBullet>().hitPoint = direction;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public GameObject bullet;
    public GameObject shootPoint;

    private void Start()
    {
        PlayerEvents.current.shooting += Shoot;
    }
    private void Shoot() {
        /*
        Quaternion fireRotation = Quaternion.LookRotation(transform.forward);

        if (Physics.Raycast(transform.position, fireRotation * Vector3.forward, out hit, Mathf.Infinity));
        GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, fireRotation);
        tempBullet.GetComponent<MoveBullet>().hitPoint = hit.point;*/
       
        GameObject tempBullet = Instantiate(bullet, shootPoint.transform.position, Quaternion.identity, transform);
        tempBullet.GetComponent<MoveBullet>().hitPoint = Controls.GetWorldMousePos();
    }


}

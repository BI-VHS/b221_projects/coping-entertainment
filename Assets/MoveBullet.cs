using System;
using System.Collections;
using System.Collections.Generic;
using NPCv2;
using UnityEngine;

public class MoveBullet : MonoBehaviour
{
    public Vector3 hitPoint;

    private Vector3 direction;

    public int damage;
    public float dispersion;
    
    private int speed = 2;
    
    // Start is called before the first frame update
    void Start()
    {
        direction.x = hitPoint.x;
        direction.y = this.transform.position.y;
        direction.z = hitPoint.z;

        var childPlayer = GameObject.FindGameObjectWithTag("Player");
        var mousePos = Controls.GetWorldMousePos();

        var lookPos = mousePos - childPlayer.transform.position;
        var rotation = Quaternion.LookRotation(lookPos);
        
        this.transform.rotation = rotation;
        this.GetComponent<Rigidbody>().AddForce((hitPoint - this.transform.position).normalized * speed);
        
   
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Npc")) {
            print("Trafil si npc ty chuj");

            collision.gameObject.GetComponent<NPCState>().health -= damage;
        }

        else if (collision.transform.name == "Player")
        {
            GameState.instance.health -= damage;
        }
        
        Destroy(this.gameObject);
    }

}
